#!/bin/bash

# script to get uniq counts while combining results of all individual TFs
# written by Sabarinathan Radhakrishnan

# read config file
sourcedir=$(dirname $BASH_SOURCE) # find the path where scripts are available
source $sourcedir/config.file # read config file to get data path variables

# usage
show_help() {
cat << EOF
Usage: ${0##*/} -c <cancerType> -t <targetAnalysis>
	-h  display this help and exit
        -c  cancer type (skcm)
        -t  target analysis 
Example: ${0##*/} -c skcm -t proximal
EOF
}
if [[ $# == 0 ]];then show_help;exit 1;fi

# set 0, if the TFBS are not seperated by binding strength
seperated=0
while getopts "d:c:t:eh" opt; do
    case "$opt" in
        h) show_help;exit 0;;
        c) ctype=$OPTARG;;
        t) target=$OPTARG;;
       '?')show_help >&2 exit 1 ;;
    esac
done

# check input
if [[ -z $ctype || -z $target ]];then 
    echo -e "\n\n\t***input missing check the usage below**\n\n"; show_help; exit 1;
fi

if [ $target == "proximal" ];then
  atypes=('DHS' 'noDHS')
  outdir="tfbs-proximal"
elif [ $target == "distal" ];then
  atypes=('DHS')
  outdir="tfbs-distal"
elif [ $target == "bound-unbound" ];then
  atypes=('boundDHS' 'boundNoDHS' 'unboundNoDHS' 'unboundNoDHSSel')
  outdir="tfbs-bound-unbound"
elif [ $target == "transcribed" ];then
  atypes=('templateStrand' 'nontemplateStrand')
  outdir="tfbs-tss-downstream"
elif [ $target == "bindStrength" ];then
  atypes=('DHS')
  outdir="tfbs-bs-seperated"
  seperated=1;
fi

echo ${atypes[@]};

# combined all TFBS results into one
if [ $seperated -eq 0 ];
then
for atype in ${atypes[@]};
do 
  # results files
  echo $atype
  respath="$path/results/$outdir/$ctype/"
  echo -e "motif_name\tctype\tposition\tbp\tcnt\tAvg" >$respath/allTFs_${atype}.csv;
  zcat $respath/*_${atype}.bed.gz | cut -f 1,3,4,6 | sort -u -S 80% | perl -sane '$bp{$F[2]}++;$cnt{$F[2]}+=$F[3];END{foreach $d (keys(%bp)){print "allTFs\t$ctype\t$d","\t",$bp{$d},"\t",$cnt{$d},"\t",($cnt{$d}/$bp{$d}),"\n";}}' -- -ctype=$ctype | sort -n -k3,3  >>$respath/allTFs_${atype}.csv
done

else
for atype in DHS;
do
   respath="$path/results/$outdir/$ctype/"
   for qrt in 0 1 2 3;
   do
   echo $qrt;
   echo -e "motif_name\tctype\tposition\tbp\tcnt\tAvg" >$respath/allTFs_${atype}_$qrt.csv; 
   zcat $respath/*_${atype}_${qrt}.bed.gz | cut -f 1,3,4,6 | sort -u -S 80% | perl -sane '$bp{$F[2]}++;$cnt{$F[2]}+=$F[3];END{foreach $d (keys(%bp)){print "allTFs\t$ctype\t",$d,"\t",$bp{$d},"\t",$cnt{$d},"\t",($cnt{$d}/$bp{$d}),"\n";}}' -- -ctype=$ctype | sort -n -k3,3  >>$respath/allTFs_${atype}_${qrt}.csv
    done
done

fi 
