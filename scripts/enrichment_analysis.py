# script to compute the mutation rate enrichment analysis
# written by Sabarinathan Radhakrishnan

# import modules
import os
import pandas as pd
import numpy as np
import scipy as sp
import scipy.stats
from math import sqrt
from math import log10
from math import log2
from statsmodels.sandbox.stats.multicomp import multipletests as mlpt

# main path
path="./"

# functions for enrichment analysis
def enrichmentTest(imutCounts, omutCounts, intsCounts, ontsCounts):
    
    # chi-square test
    chi2, p, dof, ex = sp.stats.chi2_contingency([[imutCounts, omutCounts], [(intsCounts), (ontsCounts)]], correction=False)
    # fold change
    foldChange = imutCounts/ex[0][0]
    
    return foldChange, p

def fishersTest(imutCounts, omutCounts, intsCounts, ontsCounts):
    # fisher's test
    oddsration, fisherPvalue = sp.stats.fisher_exact([[imutCounts, omutCounts], [intsCounts, ontsCounts]], alternative="greater")
    
    return fisherPvalue


# per TF enerichment analysis
# ===========================

# tf list with motif counts and size
tfList = pd.read_csv(path + "/dataset/TFBS/TFBS-DHS-Promoter_counts.txt", sep="\t", header=0)

ctypes=['skcm']#, 'brca', 'luad', 'lusc', 'crc', 'crcHyper', 'crcHypo']

inside=15

flag=0
count = 0
for ctype in ctypes:
    perTFOutput = pd.DataFrame(columns=['motif', 'ctype', 'size', 'sites', 'insideLen', 'insideRatio', 'Flank1000_Ratio', 'Flank1000_FC','Flank1000_chiPvalue' ])
    tmpCtype=ctype
    if "Hyp" in ctype:
        var = ctype[0:3]
    else:
        var = ctype
    
    for counter, row in tfList[['motif', 'size', 'counts']][ (tfList['ctype'] == var) & (tfList['counts'] > 100) ].iterrows():
        
        filename = path + "/results/tfbs-proximal/%s/%s_DHS.csv" % (ctype, row['motif'])
        
        data = pd.read_csv(filename, header=0, sep="\t", index_col=None )

        #inside=row['size']
        insideM = data['cnt'][ (data['position'] >= -inside) & (data['position'] <= inside) ].sum()
        insideN = data['bp'][ (data['position'] >= -inside) & (data['position'] <= inside) ].sum()
        arr = []
        for flank in [1000]:
        
            outsideM = data['cnt'][ (data['position'] >= -(inside + flank)) & (data['position'] < -inside) ].sum() 
            outsideM += data['cnt'][ (data['position'] > inside) & (data['position'] <= (inside+flank)) ].sum()
            outsideN = data['bp'][ (data['position'] >= -(inside + flank)) & (data['position'] < -inside) ].sum() 
            outsideN += data['bp'][ (data['position'] > inside) & (data['position'] <= (inside+flank)) ].sum()
            
            ous = [outsideM, outsideN]
            if insideM > 0 or outsideM > 0:
                fold, p = enrichmentTest(insideM, outsideM, insideN, outsideN)
            else:
                fold = 0; p = 1
            arr += [ str(ous), fold, p ]
            
        ins = [insideM, insideN]    
        
        perTFOutput.loc[count] = [ row['motif'], ctype, row['size'], row['counts'], inside, str(ins) ] +  arr
        count += 1
        
    perTFOutput['Flank1000_chiPvalueAdj']= mlpt(perTFOutput['Flank1000_chiPvalue'], method='fdr_bh')[1]
    perTFOutput['Flank1000_FC_log2'] = perTFOutput['Flank1000_FC'].apply(lambda x: log2(x) if x > 0 else log2(1))
    minPvalue = perTFOutput['Flank1000_chiPvalueAdj'][perTFOutput['Flank1000_chiPvalueAdj']!=0].min()
    perTFOutput['Flank1000_chiPvalueAdj_neglog10'] = perTFOutput['Flank1000_chiPvalueAdj'].apply(lambda x: log10(x)*-1 if x > 0 else log10(minPvalue)*-1)

    if flag == 0:
        frames = [ perTFOutput ]
        flag+=1
    else:
        frames += [ perTFOutput ]

result = pd.concat(frames)
result.to_csv(path+"/results/metafiles/enrichmentAnalysis/perTF_FC.csv", header=True, sep="\t", index=False )


# per sample enerichment analysis
# ===============================

ctypes=['skcm'] #, 'brca', 'luad', 'lusc', 'crc', 'crcHyper', 'crcHypo']

# file contains the list of mutations per samples
totMutCounts=pd.read_csv(path + "../dataset/mutations/perSample_MutationCount.txt", header=None, sep="\t", names=['ctype','sampleID','mcounts'])

inside=15 # core TF bound site

flag= 0
count = 0
for ctype in ctypes:
    perSampleOutput = pd.DataFrame(columns=['ctype', 'sample', 'totMutCounts', 'insideLen', 'insideRatio', 'Flank1000_Ratio', 'Flank1000_FC', 'Flank1000_chiPvalue'])
    # mutations mapped to all TFBS centered per sample
    filename =  path + "/results/tfbs-proximal/%s/perSample/allSampleMutationCounts_DHS.txt" % ctype
    perSample=pd.read_csv(filename, header=None, sep="\t", names=['ctype', 'pos','sample','mutation','nucl','rate'], index_col=None)

    for sample in perSample['sample'].unique():
        #print(sample)
        imutCounts = perSample['mutation'][ (perSample['pos'] >= -inside) & (perSample['pos'] <= inside) & (perSample['sample'] == sample) ].sum()
        intsCounts = perSample['nucl'][ (perSample['pos'] >= -inside) & (perSample['pos'] <= inside) & (perSample['sample'] == sample) ].sum()
        
        arr = []
        for flank in [1000]:

            omutCounts = perSample['mutation'][ (perSample['pos'] >= -flank) & (perSample['pos'] < -inside) & (perSample['sample'] == sample) ].sum() 
            omutCounts += perSample['mutation'][ (perSample['pos'] > inside) & (perSample['pos'] <= flank) & (perSample['sample'] == sample) ].sum()
            ontsCounts = perSample['nucl'][ (perSample['pos'] >= -flank) & (perSample['pos'] < -inside) & (perSample['sample'] == sample) ].sum() 
            ontsCounts += perSample['nucl'][ (perSample['pos'] > inside) & (perSample['pos'] <= flank) & (perSample['sample'] == sample) ].sum()
            ous = [omutCounts, ontsCounts]

            if imutCounts != 0 and omutCounts != 0:
                foldChange, pvalue = enrichmentTest(imutCounts, omutCounts, intsCounts, ontsCounts)
            else:
                foldChange = 0; pvalue = 1
                       
            arr += [ str(ous), foldChange, pvalue ]

        tmcnt = totMutCounts['mcounts'][totMutCounts['sampleID']==sample].values[0]    
        ins = [imutCounts, intsCounts]    
        #print(ctype, sample, tmcnt, inside, str(ins),  arr)
        perSampleOutput.loc[count] = [ ctype, sample, tmcnt, inside, str(ins) ] +  arr 
        count += 1
        
    perSampleOutput['Flank1000_chiPvalueAdj']= mlpt(perSampleOutput['Flank1000_chiPvalue'], method='fdr_bh')[1]
    perSampleOutput['Flank1000_FC_log2'] = perSampleOutput['Flank1000_FC'].apply(lambda x: log2(x) if x > 0 else log2(1))
    minPvalue = perSampleOutput['Flank1000_chiPvalueAdj'][perSampleOutput['Flank1000_chiPvalueAdj']!=0].min()
    perSampleOutput['Flank1000_chiPvalueAdj_neglog10'] = perSampleOutput['Flank1000_chiPvalueAdj'].apply(lambda x: log10(x)*-1 if x > 0 else log10(minPvalue)*-1)


    if flag == 0:
        frames = [ perSampleOutput ]
        flag+=1
    else:
        frames += [ perSampleOutput ]

result = pd.concat(frames)
result.to_csv(path+"/results/metafiles/enrichmentAnalysis/perSample_FC.csv", header=True, sep="\t", index=False )
