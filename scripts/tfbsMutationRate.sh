#!/bin/bash

# script to compute mutation rate at TFBS centered region
# written by Sabarinathan Radhakrishnan

# read configuration file
sourcedir=$(dirname $BASH_SOURCE) # find the path where scripts are available
source $sourcedir/config.file # read config file to get data path variables

show_help() {
cat << EOF
Usage: ${0##*/} -m <TFname> -c <cancertype> -t <targetAnalysis> -a <analysisType>
	-h  display this help and exit
	-m  name of TF motif (e.g., CTCFL)
	-c  cancer type (e.g., skcm)
	-t  target analysis (proximal|bound-unbound|bindStrength|distal|transcribed)
	-a  TFBS overlap with DHS or not (DHS|noDHS)
Example: ${0##*/} -m CTCFL -c skcm -t proximal -a DHS
EOF
}

if [[ $# == 0 ]];then show_help;exit 1;fi

while getopts "d:m:c:s:t:a:h" opt; do
    case "$opt" in
        h) show_help;exit 0;;
        m) motif=$OPTARG;;
        c) ctype=$OPTARG;;
        t) target=$OPTARG;;
        a) atype=$OPTARG;;
       '?')show_help >&2 exit 1 ;;
    esac
done

if [[ -z $motif || -z $ctype || -z $target || -z $atype ]];then 
   echo -e "\n\n\t***input missing check the usage below**\n\n"; show_help; exit 1;
fi

removeOverlaps=1 # if 1, remove overlapping motifs in the flanks

# make a tmp directory
if [ -d "/scratch/sabari/" ]; then 
   tmpdir=$(mktemp -d -p /scratch/sabari);
 else
   tmpdir=$(mktemp -d);
fi

# path for different input data
if [[ $target == "proximal" ]];then 
    # tfbs that're intersect with DHS/noDHS-Promoter region
    if [[ $ctype == 'luad' || $ctype == 'lusc' ]];then
        TFBSfile="$tfbsPath/proximalTFBS-${atype}_luad_lusc.bed.gz";  
    else
        TFBSfile="$tfbsPath/proximalTFBS-${atype}_${ctype}.bed.gz";  
    fi
    if [[ $ctype == 'eyelid' ]];then
        TFBSfile="$tfbsPath/proximalTFBS-${atype}_skcm.bed.gz";
    fi
    # all TFBS which will be used to filter any overlapping regions in the flank
    allTFBSmerged="$tfbsPath/merged_ENCODE.tf.bound.union.bed.gz";
    outdir="tfbs-proximal"
elif [[ $target == "bound-unbound" ]];then
    TFBSfile="$tfbsPath/allTFBS_${atype}_${ctype}.bed.gz";  
    allTFBSmerged=$TFBSfile
    outdir="tfbs-bound-unbound"
elif [[ $target == "bindStrength" ]];then
    TFBSfile="$tfbsPath/proximalTFBS-${atype}_${ctype}_quartiles.bed.gz";  
    allTFBSmerged="$tfbsPath/merged_ENCODE.tf.bound.union.bed.gz";
    outdir="tfbs-bs-seperated"
elif [[ $target == "distal" ]];then
    TFBSfile="$tfbsPath/distalTFBS-${atype}_${ctype}.bed.gz";  
    allTFBSmerged="$tfbsPath/merged_ENCODE.tf.bound.union.bed.gz"
    outdir="tfbs-distal"
elif [[ $target == "transcribed" ]];then
    TFBSfile="$tfbsPath/allTFBS_DHS_${ctype}_${atype}.bed.gz";  
    allTFBSmerged="$tfbsPath/merged_ENCODE.tf.bound.union.bed.gz"
    outdir="tfbs-tss-downstream"
else 
   echo "TFBS data not suitable";exit;
fi 

# check if the TFBS file is accessible
if [ ! -f $TFBSfile ];then echo "input TFBS file $TFBSfile not available";exit 1;fi

# create output directory
if [ ! -d "$path/results" ];then mkdir "$path/results";fi
if [ ! -d "$path/results/$outdir" ];then mkdir "$path/results/$outdir";fi
if [ ! -d "$path/results/$outdir/$ctype" ];then mkdir "$path/results/$outdir/$ctype";fi

outfile=${motif}_${atype}

if [[ $target != "bindStrength" ]] && [[ $target != "transcribed" ]];
then

# Select TFBS of interest 
# =======================
# select motifs that overlap DHS (of any degree) and promoter region (100%)
zgrep -w $motif $TFBSfile | cut -f 1-3 | sort -u | $bedtools/sortBed >$tmpdir/${outfile}_pre.bed

if [ `cat $tmpdir/${outfile}_pre.bed | wc -l` -eq 0 ];then exit;fi

# Extend TFBS from its mid-point 
# ==============================
# take +/- 1000 nucleotides from tfbs mid-point; since the intervals are zero based added one to the start position before computing mid-point
cat $tmpdir/${outfile}_pre.bed | perl -sane '$F[1]++;$midpoint=int(($F[2]+$F[1])/2);print $F[0],"\t",$midpoint,"\t",$midpoint,"\t",$F[1],"\t",$F[2],"\n";' | $bedtools/slopBed -i stdin -g $hg19genome -b $flank >$tmpdir/${outfile}_flank.bed

# Get the tri-nucleotide info.
# ============================
# first extract the sequence
perl -sane '$F[0]=~s/chr//g;$F[1]-=2;$F[2]+=1;print $F[0],"\t",$F[1],"\t",$F[2],"\t",join("|",@F),"\n";' $tmpdir/${outfile}_flank.bed | sed 's/chr//g' | $bedtools/fastaFromBed -fi $hg19Fasta -bed stdin -fo $tmpdir/seq.txt -name
# convert trinucleotides into numeric ids and remove any overlapping motifs in the flank
perl $path/scripts/makeTriNucProfile_motifs.pl TFBS $tmpdir/seq.txt $flank $allTFBSmerged 0 | sort -k1,1 -k2,2n >$tmpdir/tmp.bed

# Apply filters
# =============
# filter if any positions that overlap with CDS, blacklisted region and low mappability
$bedtools/intersectBed -wa -a $tmpdir/tmp.bed -b $blackListV1 -f 1.0 -v -sorted | $bedtools/intersectBed -wa -a stdin -b $cds -f 1.0 -v -sorted | $bedtools/intersectBed -wa -a stdin -b $mappable36mer -f 1.0 -sorted >$tmpdir/tmp_core.bed

# Compute Mutation rate 
# =====================
# map and count the number of mutations that overlap each position
$bedtools/intersectBed -c -a $tmpdir/tmp_core.bed -b $mutpath/$mutationdata/$ctype.txt.gz -f 1.0 | sort -k1,1 -k2,2n >$tmpdir/${outfile}.bed
# compute per position mutation rate
echo -e "motif_name\tctype\tposition\tbp\tcnt\tAvg" >$tmpdir/${outfile}.csv
cat $tmpdir/${outfile}.bed | perl -sane '$bp{$F[3]}++;$cnt{$F[3]}+=$F[5];END{foreach $d (keys(%bp)){print $motif,"\t",$ctype,"\t",$d,"\t",$bp{$d},"\t",$cnt{$d},"\t",($cnt{$d}/$bp{$d}),"\n";}}' -- -ctype=$ctype -motif=$motif| sort -n -k3,3 | grep -v pos >>$tmpdir/${outfile}.csv

# move the output files
gzip -9 $tmpdir/${outfile}.bed
cp $tmpdir/${outfile}.bed.gz $tmpdir/${outfile}.csv $path/results/$outdir/$ctype/.

elif [[ $target == "bindStrength" ]];
then

for quartile in 0 1 2 3;
do

outfile=${motif}_${atype}_${quartile}

# Select TFBS of interest 
# =======================
# select motifs that overlap DHS (of any degree) and promoter region (100%)
zgrep -w $motif $TFBSfile | awk -v var=$quartile '$6==var' | cut -f 1-3 | sort -u | $bedtools/sortBed >$tmpdir/${outfile}_pre.bed

if [ `cat $tmpdir/${outfile}_pre.bed | wc -l` -eq 0 ];then exit;fi

# Extend TFBS from its mid-point 
# ==============================
# take +/- 1000 nucleotides from tfbs mid-point
cat $tmpdir/${outfile}_pre.bed | perl -sane 'print $F[0],"\t",$F[1],"\t",$F[1],"\t",($F[1]-10),"\t",($F[2]+10),"\n";' | $bedtools/slopBed -i stdin -g $hg19genome -b $flank >$tmpdir/${outfile}_flank.bed

# Get the tri-nucleotide info.
# ============================
# first extract the sequence
perl -sane '$F[0]=~s/chr//g;$F[1]-=2;$F[2]+=1;print $F[0],"\t",$F[1],"\t",$F[2],"\t",join("|",@F),"\n";' $tmpdir/${outfile}_flank.bed | sed 's/chr//g' | $bedtools/fastaFromBed -fi $hg19Fasta -bed stdin -fo $tmpdir/seq.txt -name
# convert trinucleotides into numeric ids and remove any overlapping motifs in the flank
perl $path/scripts/makeTriNucProfile_motifs.pl TFBS $tmpdir/seq.txt $flank $allTFBSmerged 0 | sort -k1,1 -k2,2n >$tmpdir/tmp.bed

# Apply filters
# =============
# filter if any position that overlap with CDS, blacklisted region and low mappability
$bedtools/intersectBed -wa -a $tmpdir/tmp.bed -b $blackListV1 -f 1.0 -v -sorted | $bedtools/intersectBed -wa -a stdin -b $cds -f 1.0 -v -sorted | $bedtools/intersectBed -wa -a stdin -b $mappable36mer -f 1.0 -sorted >$tmpdir/tmp_core.bed

# Compute Mutation rate 
# =====================
# map and count the number of mutations that overlap each position
$bedtools/intersectBed -c -a $tmpdir/tmp_core.bed -b $mutpath/$mutationdata/$ctype.txt.gz -f 1.0 | sort -k1,1 -k2,2n >$tmpdir/${outfile}.bed
# compute per position mutation rate
echo -e "motif_name\tctype\tposition\tbp\tcnt\tAvg" >$tmpdir/${outfile}.csv
cat $tmpdir/${outfile}.bed | perl -sane '$bp{$F[3]}++;$cnt{$F[3]}+=$F[5];END{foreach $d (keys(%bp)){print $motif,"\t",$ctype,"\t",$d,"\t",$bp{$d},"\t",$cnt{$d},"\t",($cnt{$d}/$bp{$d}),"\n";}}' -- -ctype=$ctype -motif=$motif| sort -n -k3,3 | grep -v pos >>$tmpdir/${outfile}.csv

# move the output files
gzip -9 $tmpdir/${outfile}.bed
cp $tmpdir/${outfile}.bed.gz $tmpdir/${outfile}.csv $path/results/$outdir/$ctype/.

done

elif [[ $target == "transcribed" ]];
then

#echo "transcribed";
# Select TFBS of interest 
# =======================
# select motifs that overlap DHS (of any degree) and promoter region (100%)
zgrep -w $motif $TFBSfile | sort -u | $bedtools/sortBed >$tmpdir/${outfile}_pre.bed

if [ `cat $tmpdir/${outfile}_pre.bed | wc -l` -eq 0 ];then exit;fi

# Extend TFBS from its mid-point 
# ==============================
# take +/- 1000 nucleotides from tfbs mid-point; since the intervals are zero based added one to the start position before computing mid-point
cat $tmpdir/${outfile}_pre.bed | perl -sane '$F[1]++;$midpoint=int(($F[2]+$F[1])/2);print $F[0],"\t",$midpoint,"\t",$midpoint,"\t",join("\t",@F[1..$#F]),"\n";' | $bedtools/slopBed -i stdin -g $hg19genome -b $flank >$tmpdir/${outfile}_flank.bed

# Get the tri-nucleotide info.
# ============================
# first extract the sequence
perl -sane '$F[0]=~s/chr//g;$F[1]-=2;$F[2]+=1;print $F[0],"\t",$F[1],"\t",$F[2],"\t",join("|",@F),"\n";' $tmpdir/${outfile}_flank.bed | sed 's/chr//g' | $bedtools/fastaFromBed -fi $hg19Fasta -bed stdin -fo $tmpdir/seq.txt -name
# convert trinucleotides into numeric ids and remove any overlapping motifs in the flank
perl $path/scripts/makeTriNucProfile_motifs.pl TFBS $tmpdir/seq.txt $flank $allTFBSmerged 1 | sort -k1,1 -k2,2n >$tmpdir/tmp.bed

# Apply filters
# =============
# filter if any positions that overlap with CDS, blacklisted region and low mappability
$bedtools/intersectBed -wa -a $tmpdir/tmp.bed -b $blackListV1 -f 1.0 -v -sorted | $bedtools/intersectBed -wa -a stdin -b $cds -f 1.0 -v -sorted | $bedtools/intersectBed -wa -a stdin -b $mappable36mer -f 1.0 -sorted >$tmpdir/tmp_core.bed

# Compute Mutation rate 
# =====================
# map and count the number of mutations that overlap each position
$bedtools/intersectBed -c -a $tmpdir/tmp_core.bed -b $mutpath/$mutationdata/$ctype.txt.gz -f 1.0 | sort -k1,1 -k2,2n >$tmpdir/${outfile}.bed
# compute per position mutation rate
echo -e "motif_name\tctype\tposition\tbp\tcnt\tAvg" >$tmpdir/${outfile}.csv
cat $tmpdir/${outfile}.bed | perl -sane '$bp{$F[3]}++;$cnt{$F[3]}+=$F[5];END{foreach $d (keys(%bp)){print $motif,"\t",$ctype,"\t",$d,"\t",$bp{$d},"\t",$cnt{$d},"\t",($cnt{$d}/$bp{$d}),"\n";}}' -- -ctype=$ctype -motif=$motif| sort -n -k3,3 | grep -v pos >>$tmpdir/${outfile}.csv

# move the output files
gzip -9 $tmpdir/${outfile}.bed
cp $tmpdir/${outfile}.bed.gz $tmpdir/${outfile}.csv $path/results/$outdir/$ctype/.


fi

#remove the tmp directory
rm -rf $tmpdir
