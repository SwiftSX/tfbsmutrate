#!/usr/bin/perl -w

# script to collect the result of per sample mutation rate analysis in a metafile
# written by Sabarinathan Radhakrishnan

use strict;

my $ctype=$ARGV[0];
my $atype=$ARGV[1];
my $path=$ARGV[2];
my $perSamplePath=$ARGV[3];

open(IN,"$perSamplePath/$atype\_nonmutated.cnt");
my %cnt;
my %bp;

#initialize
for(my $i=-1000;$i<=1000;$i++)
{
  $cnt{$i}->{"notmutated"}=0;
  $bp{$i}->{"notmutated"}=0;
}

while(<IN>)
{
  my @s=split(/\s+/, $_);
  $cnt{$s[1]}->{"notmutated"}=$s[2];
  $bp{$s[1]}->{"notmutated"}=$s[3];
}
close IN;

#my @arr=`ls $perSamplePath/$ctype/TCGA*\_$atype.cnt`;
my @arr=`ls $perSamplePath/*\_$atype.cnt`;
my @ids;
foreach my $file (@arr)
{
  chomp $file;
  my $tt=(split /\//,  $file)[-1];
  my $name=(split /\_/,  $tt)[0];
  #initialize
  for(my $i=-1000;$i<=1000;$i++) {
  $cnt{$i}->{$name}=0; $bp{$i}->{$name}=0;
  }
  open(IN,$file); 
  while(<IN>)
  {
  my @s=split(/\s+/, $_);
  $cnt{$s[1]}->{$name}=$s[2];
  $bp{$s[1]}->{$name}=$s[3];
  }
  close IN;
  push(@ids, $name);
}

if($ctype=~/skcm/)
{
 open(IN,"$path/results/tfbs-proximal/eyelid/allTFs_$atype.csv");
 my $name="normalSkin";
 for(my $i=-1000;$i<=1000;$i++) {
  $cnt{$i}->{$name}=0; $bp{$i}->{$name}=0;
  }
 while(<IN>)
 {
  my @s=split(/\s+/, $_);  
  next, if($_=~/^motif_name/);
  $cnt{$s[2]}->{$name}=$s[4];
  $bp{$s[2]}->{$name}=$s[3];
 }
 close IN;
 push(@ids, $name);
} 


open(OUT1,">$perSamplePath/allSampleMutationCounts_$atype.txt");
if(! -d "$path/results/metafiles/tfbs-perSample"){`mkdir $path/results/metafiles/tfbs-perSample`;}
open(OUT2,">$path/results/metafiles/tfbs-perSample/$ctype\_perSample\_$atype.csv");

print OUT2 "position","\t",join("\t",@ids),"\n";

foreach my $key (sort {$a <=> $b} keys(%bp))
{
 print OUT2 $key;
 foreach my $id (@ids)
 {
   my $mrate;
   if ($id=~/normalSkin/)
   {
    $mrate = ($cnt{$key}->{$id})/($bp{$key}->{$id});
    print OUT1 "$ctype\t$key\t$id\t",($cnt{$key}->{$id}),"\t",($bp{$key}->{$id}),"\t",$mrate,"\n";
   }
   else
   {
    $mrate = ($cnt{$key}->{$id}+$cnt{$key}->{"notmutated"})/($bp{$key}->{$id}+$bp{$key}->{"notmutated"});
    print OUT1 "$ctype\t$key\t$id\t",($cnt{$key}->{$id}+$cnt{$key}->{"notmutated"}),"\t",($bp{$key}->{$id}+$bp{$key}->{"notmutated"}),"\t",$mrate,"\n";
   }
   print OUT2 "\t",$mrate;
   #print OUT1 "$key\t$id\t",$cnt{$key}->{$id},"\t",$bp{$key}->{$id},"\n";
 }
 print OUT2 "\n";
}
close OUT1;
close OUT2;
`gzip -f $path/results/metafiles/tfbs-perSample/$ctype\_perSample\_$atype.csv`;
