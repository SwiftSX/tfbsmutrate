#!/bin/bash

# program to get uniq counts of nucleosome position signal while combining results of all individual TFBS
# written by Sabarinathan Radhakrishnan

sourcedir=$(dirname $BASH_SOURCE) # find the path where scripts are available
source $sourcedir/config.file # read config file to get data path variables

# usage
show_help() {
cat << EOF
Usage: ${0##*/} -c <cancerType> -t <targetAnalysis> 
	-h  display this help and exit
        -c  cancer type (skcm)
        -t  target analysis (distal)
Example: ${0##*/} -c skcm -t distal
EOF
}
if [[ $# == 0 ]];then show_help;exit 1;fi

# read input
expression=0
while getopts "c:t:h" opt; do
    case "$opt" in
        h) show_help;exit 0;;
        c) ctype=$OPTARG;;
        t) target=$OPTARG;;
       '?')show_help >&2 exit 1 ;;
    esac
done

# check input
if [[ -z $ctype || -z $target ]];then 
    echo -e "\n\n\t***input missing check the usage below**\n\n"; show_help; exit 1;
fi


# combined all TFBS results into one
# results files
respath="$path/results/tfbs-$target/$ctype/nucleosome/"
echo -e "motif_name\tctype\tposition\tbp\tcnt\tAvg" >$respath/allTFs_${ctype}.csv;
zcat $respath/*_${ctype}.bed.gz | cut -f 1,3,4,10 | sort -u -S 80% | perl -sane '$bp{$F[2]}++;$cnt{$F[2]}+=$F[3];END{foreach $d (keys(%bp)){print "allTFs\t$ctype\t$d","\t",$bp{$d},"\t",$cnt{$d},"\t",($cnt{$d}/$bp{$d}),"\n";}}' -- -ctype=$ctype | sort -n -k3,3  >>$respath/allTFs_${ctype}.csv
