#!/bin/bash

# script to get uniq counts of xr-seq while combining results of all individual TFBS
# written by Sabarinathan Radhakrishnan

sourcedir=$(dirname $BASH_SOURCE) # find the path where scripts are available
source $sourcedir/config.file # read config file to get data path variables

# usage
show_help() {
cat << EOF
Usage: ${0##*/} -c <cancerType> -t <targetAnalysis> 
	-h  display this help and exit
        -c  cancer type (skcm)
        -t  target analysis (proximal)
Example: ${0##*/} -c skcm -t proximal
EOF
}
if [[ $# == 0 ]];then show_help;exit 1;fi

# read input
while getopts "c:t:h" opt; do
    case "$opt" in
        h) show_help;exit 0;;
        c) ctype=$OPTARG;;
        t) target=$OPTARG;;
       '?')show_help >&2 exit 1 ;;
    esac
done

# check input
if [[ -z $ctype || -z $target ]];then 
    echo -e "\n\n\t***input missing check the usage below**\n\n"; show_help; exit 1;
fi

# target matches
declare -A tmatch=(["proximal"]="tfbs-proximal" ["distal"]="tfbs-distal" ["bindStrength"]="tfbs-bs-seperated" ["transcribed"]="tfbs-tss-downstream")
target=${tmatch[$target]} 

# analysis types
atypes=('DHS')
if [ $target == "bound-unbound" ];then
  atypes=('boundDHS' 'boundNoDHS' 'unboundNoDHSSel')
elif [ $target == "tfbs-tss-downstream" ];then
  atypes=('templateStrand' 'nontemplateStrand')
fi

# xrseqcell type 
xcelltypes=('CPD1h' 'CSBCPD' 'PP641h' 'CSB64')
if [ $target == "tfbs-tss-downstream" ];
then 
  xcelltypes=('XPC64' 'XPCCPD')
fi


###### for DHS overlapping regions
# combined all TFBS results into one
if [[ $target != "tfbs-bs-seperated" ]]; # if it is not expression seperated
then
for pp in ${xcelltypes[@]};
do 
    # for each analysis types
    for atype in ${atypes[@]};
    do
     # results files
     respath="$path/results/$target/$ctype/xr-seq/$pp"
     for strand in PLUS MINUS;
     do
      echo -e "$pp\t$strand";
      echo -e "motif_name\tposition\tbp\tcnt\tAvg" >$respath/allTFs_${atype}_${pp}_${strand}.csv;
      zcat $respath/*_${atype}_${pp}_${strand}.bed.gz | cut -f 1,3,4,10 | sort -u -S 80% | perl -sane '$bp{$F[2]}++;$cnt{$F[2]}+=$F[3];END{foreach $d (keys(%bp)){print "$ctype\t",$d,"\t",$bp{$d},"\t",$cnt{$d},"\t",($cnt{$d}/$bp{$d}),"\n";}}' -- -ctype=$ctype | sort -n -k2,2  >>$respath/allTFs_${atype}_${pp}_${strand}.csv
     done
    done
done

else # if it is expression seperated
for pp in ${xcelltypes[@]};
do 
  # results files
  respath="$path/results/$target/$ctype/xr-seq/$pp"
  atype="DHS"
  for strand in PLUS MINUS;
  do
    for qrt in 0 1 2 3;
    do
    echo -e "$pp\t$strand";
    echo -e "motif_name\tposition\tbp\tcnt\tAvg" >$respath/allTFs_${atype}_${qrt}_${pp}_${strand}.csv;
    zcat $respath/*_${atype}_${qrt}_${pp}_${strand}.bed.gz | cut -f 1,3,4,10 | sort -u -S 80% | perl -sane '$bp{$F[2]}++;$cnt{$F[2]}+=$F[3];END{foreach $d (keys(%bp)){print "$ctype\t",$d,"\t",$bp{$d},"\t",$cnt{$d},"\t",($cnt{$d}/$bp{$d}),"\n";}}' -- -ctype=$ctype | sort -n -k2,2  >>$respath/allTFs_${ctype}_${qrt}_${pp}_${strand}.csv
    done
  done
done

fi
