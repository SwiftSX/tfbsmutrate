#!/usr/bin/env python3 

# script to collect randomised mutations in a metafile
# written by Sabarinathan Radhakrishnan

import os
import sys
import numpy as np
import pandas as pd
import glob
import scipy as sp
import scipy.stats
import gzip
import argparse
import logging
from multiprocessing import Pool, cpu_count

# paths
path="/projects_bg/bg/users/sabari/noncoding/testing/tfbsmutrate"

# Parse command line arguments
parser = argparse.ArgumentParser()
parser.add_argument('-c', dest='ctype', help='cancer types (e.g skcm)')
parser.add_argument('-t', dest='target', help='target analysis (e.g proximal)')
parser.add_argument('-a', dest='atype', help='analysis type (DHS or noDHS)')
parser.add_argument('-o', dest='outdir', help='output directory')
parser.add_argument('--cores', dest='cores', type=int, default=os.cpu_count(), help='Maximum number of CPU cores to use')
parser.add_argument('--debug', dest='debug', action='store_true')
args = parser.parse_args()

if args.ctype == None or args.atype == None or args.target == None or args.outdir == None:
    parser.print_usage()
    sys.exit(1)

# set output path
opath = "%s/results/%s" % (path, args.outdir) # output path

# default params
rand=1000 # number of random sampling performed 
flank=1000 # flanking from the TFBS mid-point 

# save the read files for further analysis
allmotifsBS={}
allmotifsBG={}

### functions
def mean_confidence_interval(line):
    confidence=0.95
   # data = [line['rand_' + str(i)] for i in range(1, (rand+1))]
    data = line
    n = len(data)
    m, se, sd = np.mean(data), sp.stats.sem(data), np.std(data)
    h = se * sp.stats.t._ppf((1 + confidence) / 2, n-1) # confidence interval
    return m, m - h, m + h, sd

def extractDetails(sfile):
    """ extract details for per TF """
    # extract the tf name from the filename
    tf = sfile.split("/")[-1].split("_")[0]

    # choose the observed mutation rate file to get the number of binding sites in each position for normalization
    pos={}
    motif = pd.read_csv(sfile, sep="\t", header=0)
    # create the dictionary to save the number of binding sites for each position
    motifBS = dict(zip(motif['position'],motif['bp']))
 
    # background file name
    mfilename="%s/results/%s/%s/background/%s_%s_rand.bin.gz" % (path, args.target, args.ctype, tf, args.atype)
    # unzip and read the background file
    a = np.fromstring(gzip.open(mfilename, "rb").read(), dtype="int32")

    #mfilename="/projects_bg/bg/shared/projects/transcriptionfactors/output/run2/%s/%s/%s_rand.bin" % (ctype,atype.lower(),tf)
    #a = np.fromfile(mfilename, dtype='int32')

    finalDic={}
    #compute the average for each random sampling
    for sam in range(0, rand):
        # intialize array to store the mutation position counts for each sampling
        for i in range(-flank, flank+1):
            pos[i]=0

        # read through each TF files and extract the output for one sampling
        start = int((len(a)/rand)*sam)
        count = np.unique(a[start:(start+int(len(a)/rand))], return_counts=True) # count for one random sampling
        for i in range(0, len(count[0])):
            pos[count[0][i]] += count[1][i]

        # normalize the counts with total binding sites
        for cnt, i in enumerate(range(-flank, flank+1)):
            if i in motifBS and motifBS[i] != 0:
                pos[i] = pos[i]/motifBS[i]
            else:
                pos[i] = 0
            if not "rand_{}".format(sam + 1) in finalDic.keys():
                finalDic["rand_{}".format(sam + 1)] = {i:pos[i]}
            else:
                finalDic["rand_{}".format(sam + 1)].update({i:pos[i]})

    # save the final output for each of the 1000 random sampling
    finalOutput=pd.DataFrame.from_dict(finalDic,orient='columns')
    #finalOutput['position']=finalOutput.index

    # compute mean, confidence interval and standard deviation
    t = finalOutput.apply(mean_confidence_interval, axis=1)

    return(tf, t, motifBS, a)

def extractDetailsAllTFs():
    """ extract details for all TFs together """
    finalDic={}
    #compute the average for each random sampling
    for sam in range(0, rand):
        pos={}
        bindsites={}
        # intialize array to store the mutation position counts for each sampling
        for i in range(-flank, flank+1):
            pos[i]=0
            bindsites[i]=0

        for tf in allmotifsBS.keys():
            # read through each TF files and extract the output for one sampling
            start = int((len(allmotifsBG[tf])/rand)*sam)
            count = np.unique(allmotifsBG[tf][start:(start+int(len(allmotifsBG[tf])/rand))], return_counts=True) # count for one random sampling
            for i in range(0, len(count[0])):
                pos[count[0][i]] += count[1][i]

            for i in range(-flank, flank+1):
                if i in allmotifsBS[tf]:
                    bindsites[i] += allmotifsBS[tf][i]

        # normalize the counts with total binding sites and save in finalDic
        for cnt, i in enumerate(range(-flank, flank+1)):
            if i in bindsites and bindsites[i] != 0:
                pos[i] = pos[i]/bindsites[i]
            else:
                pos[i] = 0
            if not "rand_{}".format(sam + 1) in finalDic.keys():
                finalDic["rand_{}".format(sam + 1)] = {i:pos[i]}
            else:
                finalDic["rand_{}".format(sam + 1)].update({i:pos[i]})

    # save the final output for each of the 1000 random sampling
    finalOutput=pd.DataFrame.from_dict(finalDic,orient='columns')
    #finalOutput['position']=finalOutput.index
    
    # compute mean, confidence interval and standard deviation
    t = finalOutput.apply(mean_confidence_interval, axis=1)
    return(t)
   
# collect outputs
backgPerTF={}
# get the filenames of all TFs expect the allTFs results together
sfiles=[ fn for fn in glob.glob('%s/results/%s/%s/*_%s.csv' % (path, args.target, args.ctype, args.atype)) if not os.path.basename(fn).startswith('allTFs') ]

# Configure the logging
level = logging.DEBUG if args.debug else logging.INFO
logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', datefmt='%H:%M:%S', level=level)
logging.debug(args)

# run each TF in parallel
print("processing per TF files")
pool=Pool(processes=args.cores)

logging.debug("Start")
for results in pool.map(extractDetails, sfiles):
    # extract the tf name from the filename
    tf = results[0] # TF name
    t = results[1] # results
    backgPerTF["%s_mean" % tf] = [i[0] for i in t]
    backgPerTF["%s_CIL" % tf] = [i[1] for i in t]
    backgPerTF["%s_CIU" % tf] = [i[2] for i in t]
    backgPerTF["%s_sd" % tf] = [i[3] for i in t]

    allmotifsBS[tf] = results[2] # info about #of binding sites
    allmotifsBG[tf] = results[3] # background information
logging.debug("Done")
# for all TFBS together
print("processing all TF files together")
t = extractDetailsAllTFs()
tf = "allTFs"
backgPerTF["%s_mean" % tf] = [i[0] for i in t]
backgPerTF["%s_CIL" % tf] = [i[1] for i in t]
backgPerTF["%s_CIU" % tf] = [i[2] for i in t]
backgPerTF["%s_sd" % tf] = [i[3] for i in t]

# save the final output in a dataframe to print
toprint = pd.DataFrame.from_dict(backgPerTF, orient='columns')
position = [ val for val in range(-flank, flank+1) ]
toprint.insert(0, 'position', position)
outfile = "%s/%s_TFBS_%s_backg.csv" % (opath, args.ctype, args.atype)
toprint.to_csv(outfile,header=True,sep="\t",index=False)
os.system("gzip %s" % outfile)
