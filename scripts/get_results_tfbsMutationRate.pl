#!/usr/bin/perl -w

# script to generate a metafile output with mutation rate for each TF column wise and one column for all TFBS together
# this output file is used an input to plot the figures
# written by Sabarinathan Radhakrishnan

use strict;
use Getopt::Std;

sub usage(){print "$0 -c <cancerType> -t <targetAnalysis> -a <analysisType>\n";exit;}

my %opts;
getopt('cta',\%opts);

print scalar(keys(%opts)),"\n";
&usage, if(scalar(keys(%opts))>4 || scalar(keys(%opts))<3);

my $path="./"; # parent directory
my $ctype=$opts{c}; # cancer type
my $type=$opts{a}; # DHS or noDHS
my $target=$opts{t}; # target analysis 
my $moutput=$opts{o}; # output metafile directory

my $seperated=0; #tfbs seperated based on binding strength
if ($target=~/bindStrength/){$seperated=1;}

my $flank=1000;

# output directories match
my %outdir=qw(proximal tfbs-proximal distal tfbs-distal bound-unbound tfbs-bound-unbound transcribed tfbs-tss-downstream bindStrength tfbs-bs-seperated);

# create a metafile folder
if (!-d "$path/results/metafiles"){`mkdir $path/results/metafiles`;}

# create a target analysis output directory inside metafile folder
my $opath="$path/results/metafiles/$outdir{$target}"; # output pat
if (!-d $opath){`mkdir $opath`;print "creating an output directory\n";}

# select the results depends on the given type DHS or noDHS
my @files=`ls $path/results/$outdir{$target}/$ctype/*.csv | grep "_$type"`;

my %hash;my %tf;
foreach my $file (@files)
{
  chomp $file;
  open(IN,$file) or die "couldn't access to the file $file\n";
  while(<IN>)
  {
    next, if($_=~/^motif_name/);
    my @s=split(/\s+/, $_);
    my $name;
    if($seperated==0)
    { $name=$s[0]; }
    else{ 
    my $qrt=(split(/\./, (split(/\_/, $file))[-1]))[0];
    $name="$s[0]_$qrt";
    }
    $hash{$s[2]}->{$name}=$s[5];
    $tf{$name}=0;
  }
}

open(OUT,">$opath/$ctype\_TFBS_$type.csv");
# print header
print OUT "position";
foreach my $motif (sort(keys %tf))
{ print OUT "\t$motif"; }
#print OUT "\tallTFs\n";
print OUT "\n";

# print values
for(my $i=-$flank;$i<=$flank;$i++)
{
 my $val="$i";
 foreach my $motif (sort(keys %tf))
 {
  if(exists $hash{$i}->{$motif}){ $val.="\t".$hash{$i}->{$motif}; } 
  else{$val.= "\t0";}
 }
 print OUT $val,"\n";
}
close OUT;

# by default it gzip the outfile
`gzip -f $opath/$ctype\_TFBS_$type.csv`;
