#!/bin/bash

# script to compute background mutation rate for TFBS centered analysis
# written by Sabarinathan Radhakrishnan

sourcedir=$(dirname $BASH_SOURCE) # find the path where scripts are available
source $sourcedir/config.file # read config file to get data path variables

show_help() {
cat << EOF
Usage: ${0##*/} -m <TFname> -c <cancertype> -t <targetAnalysis> -a <analysisType> -n <number of cores>
	-h  display this help and exit
	-m  name of TF motif (e.g., CTCFL)
	-c  cancer type (e.g., skcm)
	-t  target analysis (e.g., proximal|distal)
	-a  analysis types (DHS|noDHS)
	-n  number of cores (default:4)
Example: ${0##*/} -m CTCFL -c skcm -t proximal -a DHS
EOF
}

if [[ $# == 0 ]];then show_help;exit 1;fi

background=0
while getopts "m:c:t:a:n:h" opt; do
    case "$opt" in
        h) show_help;exit 0;;
        m) motif=$OPTARG;;
        c) ctype=$OPTARG;;
        t) target=$OPTARG;;
        a) analysis=$OPTARG;;
        n) ncpus=$OPTARG;;
       '?')show_help >&2 exit 1 ;;
    esac
done

# check input variables
if [[ -z $motif || -z $ctype || -z $target || -z $analysis ]];then 
   echo -e "\n\n\t***input missing check the usage below**\n\n"; show_help; exit 1;
fi

# assign number of cores
if [[ -z $ncpus ]];then ncpus=4;fi

flank=1000 # flank from the motif mid-point
outfile=${motif}_${analysis}
mutprob="$mutpath/signature_probabilities.tsv"

# source path (ie., the motif file which has observed mutations mapped)
srcpath="$path/results/tfbs-$target/$ctype/"

# check if the input source file exits
if [ ! -f "$srcpath/${outfile}.bed.gz" ];then
   echo -e "input file not available here $srcpath/${outfile}.bed.gz";exit 1;
fi
if [ ! -f $mutprob ];then
   echo -e "mutation probability file not available here $mutprob";exit 1;
fi

# assign which column to choose in the mutation probability file
if [[ $analysis == "DHS" || $analysis == "boundDHS" ]];then col="DHS";fi
if [[ $analysis == "noDHS" ||  $analysis == "boundNoDHS" || $analysis == "unboundNoDHS" ]];then col="noDHS";fi

# make a tmp directory
if [ -d "/scratch/" ]; then 
   tmpdir=$(mktemp -d -p /scratch/)
 else
   tmpdir=$(mktemp -d)
fi

# output path, the path where to store the final output
outpath="$srcpath/background"

# create output directory
if [ ! -d "$outpath" ];then mkdir "$outpath";fi

# Compute background Mutation rate 
# ===============================
# copy the observed mutation file to tmp directory
cp $srcpath/${outfile}.bed.gz $tmpdir/.
gzip -d $tmpdir/${outfile}.bed.gz

mkdir $tmpdir/$motif
# seperate hits for per TFBS
for chr in $(cut -f1 $tmpdir/${outfile}.bed | sort -u);do grep -w $chr $tmpdir/${outfile}.bed | sort -n -k2,2 | perl -sane 'BEGIN{$flag=0;$tmp=0;};if($flag==0){open(OUT,">$tmpdir/$motif\_$F[0]\_$F[1].bed");}if(($F[1]-$tmp)>1000 && $flag>0){close OUT;open(OUT,">$tmpdir/$motif\_$F[0]\_$F[1].bed");print OUT $_;}else{print OUT $_;}$tmp=$F[1];$flag++;END{close OUT;}' -- -tmpdir="$tmpdir/$motif" -motif="$motif";done

# perform the background mutation rate computation, script from Jordi Deu-Pons
python $path/scripts/samplingMutation_v01.py -c Probability_${ctype}_${col}PROM -s $mutprob -f $tmpdir/$motif  -o $tmpdir/ -p "*[0-9].bed" --cores=$ncpus

# move the output files
gzip -9 $tmpdir/rand.bin
mv $tmpdir/rand.bin.gz $outpath/${outfile}_rand.bin.gz

# remove the tmp directory
rm -rf $tmpdir
