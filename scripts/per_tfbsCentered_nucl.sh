#!/bin/bash

# program to map the nucleosome positioning results into the TFBS centered analysis
# written by Sabarinathan Radhakrishnan

# read configuration file
sourcedir=$(dirname $BASH_SOURCE) # find the path where scripts are available
source $sourcedir/config.file # read config file to get data path variables

show_help() {
cat << EOF
Usage: ${0##*/} -m <TFname> -c <ctype> -t <targetAnalysis> -a <analysisType>
	-h  display this help and exit
	-m  motif name (CTCF)
	-s  name of mutation data set (tcga505)
        -c  cancer type (skcm)
        -t  target analysis (distal)
        -a  analysis type (DHS)
Example: ${0##*/} -m ATF3 -c skcm -t distal -a DHS
EOF
}

if [[ $# == 0 ]];then show_help;exit 1;fi

while getopts "m:t:c:a:h" opt; do
    case "$opt" in
        h) show_help;exit 0;;
        m) tf=$OPTARG;;
        t) target=$OPTARG;;
        c) ctype=$OPTARG;;
        a) atype=$OPTARG;;
       '?')show_help >&2 exit 1 ;;
    esac
done

if [[ -z $tf || -z $target || -z $ctype || -z $atype ]];then 
   echo -e "\n\n\t***input missing check the usage below**\n\n"; show_help; exit 1;
fi


# make a tmp directory
if [ -d "/scratch/" ];
 then 
   tmpdir=$(mktemp -d -p /scratch/);
 else
   tmpdir=$(mktemp -d);
fi

nuclPath="$path/dataset/nucleosome" # path for nucleosome data

# output directory
if [ ! -d "$path/results/tfbs-$target/$ctype/nucleosome" ];then mkdir "$path/results/$dataset/tfbs-$target/$ctype/nucleosome";fi

opath="$path/results/tfbs-$target/$ctype/nucleosome"
tfbsopath="$path/results/tfbs-$target/$ctype"

if [ ! -f $tfbsopath/${tf}_${atype}.bed.gz ];then echo "tfbs file not exists in the path $tfbsopath/${tf}_${atype}.bed.gz";exit;fi
zcat $tfbsopath/${tf}_${atype}.bed.gz | perl -sane '$F[1]--;print join("\t",@F),"\n";' >$tmpdir/${tf}_${atype}.bed

## mapping
$bedtools/intersectBed -wo -a $tmpdir/${tf}_${atype}.bed -b $nuclPath/wgEncodeSydhNsomeGm12878Sig.bed.gz -f 1.0 -sorted >$tmpdir/${tf}_${ctype}.bed
## compute the nucleosome signal
echo -e "motif_name\tposition\tbp\tcnt\tAvg" >$tmpdir/${tf}_${ctype}.csv;
cat $tmpdir/${tf}_${ctype}.bed | perl -sane '$bp{$F[3]}++;$cnt{$F[3]}+=$F[9];END{foreach $d (keys(%bp)){print "$ctype\t",$d,"\t",$bp{$d},"\t",$cnt{$d},"\t",($cnt{$d}/$bp{$d}),"\n";}}' -- -ctype=$ctype | sort -n -k2,2  >>$tmpdir/${tf}_${ctype}.csv;

### gzip and copy the results
mv $tmpdir/*.csv $opath/.
rm -rf $tmpdir/${tf}_${atype}.bed
gzip -9 $tmpdir/${tf}_${ctype}.bed
mv $tmpdir/${tf}_${ctype}.bed.gz $opath/.

rm -rf $tmpdir
