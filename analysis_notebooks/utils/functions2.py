import os
import pandas as pd
import numpy as np
import scipy as sp
import scipy.stats
from brewer2mpl import brewer2mpl
from math import sqrt
from statsmodels.sandbox.stats.multicomp import multipletests as mlpt
import rpy2.robjects as robjects


# plot parameters
from matplotlib import pyplot as plt
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from matplotlib.ticker import LogFormatter
from matplotlib import ticker

# for y-axis formating 
formatter = ticker.ScalarFormatter(useMathText=True)
formatter.set_scientific(True) 
formatter.set_powerlimits((-1,1)) 

def plotParam():
    plt.rcParams['font.sans-serif'] = ['arial']
    plt.rcParams['font.size'] = 10
    plt.rcParams['font.family'] = ['sans-serif']
    plt.rcParams['svg.fonttype'] = 'none'
    plt.rcParams['mathtext.fontset'] = 'custom'
    plt.rcParams['mathtext.cal'] = 'arial'
    plt.rcParams['mathtext.rm'] = 'arial' 

import brewer2mpl
bmap = brewer2mpl.get_map('Set1', 'qualitative', 7)
LineColors = bmap.mpl_colors


# assign colors dor different analysis
# colors for the fold enrichment plot
colors = { 'HFCsig':'#ca0020' , 'HFCnosig':'#f4a582', 'LFCsig':'#0571b0', 'LFCnosig':'#92c5de', 'other':'white' }
# colors for xr-seq photo different product and different genomic strand
# for combined strand
ptitle = {'CPD':'Average CPD repair', 'PP64':'Average (6-4)PP repair' }
# for combined strand
FGcolor = {'CPD':LineColors[1], 'PP64':LineColors[3], 'XPCCPD':LineColors[1], 'XPC64':LineColors[3], 'CSBCPD':LineColors[1], 'CSB64':LineColors[3] }
# for strand specific
xrseqFGcolor = {'CPD':{'PLUS':'#225ea8', 'MINUS':'#41b6c4'}, 'PP64':{'PLUS':'#ae017e', 'MINUS':'#df65b0'}}
# cell type Names
cellTitle = {'CSB':'CS-B', 'XPC':'XP-C', 'NHF1':'NHF1'}
motif = 13 # avergae motif size
motifColor = '#ff0099' # motif color
# for binding sites seperated
qrtName={0:'Low \n (n = 6,210)', 1:'Low-medium \n (n = 17,086)', 2:'Medium-high \n (n = 26,231)', 3:'High \n (n = 29,025)'}
strName={'PLUS':'forward', 'MINUS':'reverse'}

class analysis2:
    
    #### functions to read input files        
    def read_TFBSmutrate_tss(self, target):
        """read mutation data in TFBS downstream of TSS"""
        self.mutRateTSS= pd.read_csv("metafiles/tfbs-tss-downstream/skcm_TFBS_%s.csv.gz" % (target), header=0, sep="\t", compression='gzip', index_col=False)
   
    def read_TFBSxrseq_tss(self, target, cellType, pp):
        """read excision repair data in TFBS downstream of TSS"""
        self.repairTSS = pd.read_csv("metafiles/tfbs-tss-downstream/xr-seq/TFBS_%sStrand_%s_%s_combined.csv.gz" % (target, cellType, pp), header=0, sep="\t", compression='gzip', index_col=False)
        
    def read_TFBS_MutRate_unbound(self, ctype):
        """read mutation data for bound and unbound TFs"""
        self.mut_TFBS_type={}
        for atype in ['boundDHS', 'boundNoDHS', 'unboundNoDHS', 'unboundNoDHSSel']:
            self.mut_TFBS_type[atype] = pd.read_csv("metafiles/tfbs-bound-unbound/%s_TFBS_%s.csv.gz" % (ctype, atype), header=0, sep="\t", index_col=False, compression='gzip')
        
        self.bckg_TFBS_type={}
        for atype in ['boundDHS', 'boundNoDHS', 'unboundNoDHS']:
            self.bckg_TFBS_type[atype]=pd.read_csv("metafiles/tfbs-bound-unbound/%s_TFBS_%s_backg.csv.gz"  % (ctype, atype), header=0, sep="\t", index_col=False, compression='gzip')
            
    def read_TFBS_MutRate(self, ctype, target):
        """read mutation rate data of TFBS centered analysis"""    
        # observed somatic mutation rate
        self.mut_TFBS_DHS = pd.read_csv("metafiles/%s/%s_TFBS_DHS.csv.gz" % (target, ctype), header=0, sep="\t", compression='gzip', index_col=False) 
        
        if target == 'tfbs-proximal':
            self.mut_TFBS_NODHS = pd.read_csv("metafiles/%s/%s_TFBS_noDHS.csv.gz" % (target, ctype), header=0, sep="\t", compression='gzip', index_col=False)
            #print("%s/metafiles2/%s/%s_TFBS_DHS.csv.gz"  % (dataset, target, ctype))

    def read_TFBS_MutRate_perMutType(self, ctype):
        """read mutation rate data of TFBS centered analysis seperated by mutation type"""
        self.perMutType_TFBS_DHS = pd.read_csv("metafiles/tfbs-perMutTypes/%s_mutTypes.csv.gz" % (ctype), header=0, sep="\t", index_col=False, compression='gzip')
    
    def read_TFBS_MutRate_perMutType_detailed(self, ctype):
        """read mutation rate data of TFBS centered analysis seperated by mutation type or signature"""
        self.perMutType_TFBS_DHS = pd.read_csv("metafiles/tfbs-perMutTypes/%s_mutTypes_detailed.csv" % (ctype), header=0, sep="\t", index_col=False)
        
    def read_additionalFiles(self):
        # tf list with motif counts and size
        self.tfList = pd.read_csv("metafiles/tf_list_withMotifSize_allCtypes.txt", sep="\t", header=None, names=['motif', 'ctype', 'size', 'counts'])
        
    def read_signatureEnrichment(self):
        self.signature = pd.read_csv("metafiles/enrichmentAnalysis/mutationSignature_analysis.csv", sep="\t", header=0)
    
    #### functions to plot graphs
    def plot_TFBS_MutRate_unbound(self, axss, ctype, flank, plot, legend):
        """ function to plot mutation rate for TFBS centered analysis
        """
        scale = { 1000:200, 100:20, 400:200 } # xaxis-scale range
        mscale = { 1000:8, 100:8, 400:5 } # size to highligh core TFBS bar on x-axis
        labelSize = { 1000:10, 100:10, 400:10 }

        linetypes={'boundDHS':'-', 'boundNoDHS':"--", 'unboundNoDHS':"-."}
        linecolors={'boundDHS':LineColors[0], 'boundNoDHS':LineColors[1], 'unboundNoDHS':LineColors[4], 'unboundNoDHSSel':LineColors[6]}
        sites={'boundDHS':'20,009', 'boundNoDHS':'44,235', 'unboundNoDHS':'1,049,631', 'unboundNoDHSSel':'20,009'}
        labels={'boundDHS':'bound-DHS', 'boundNoDHS':'bound-noDHS', 'unboundNoDHS':'unbound-noDHS all', 'unboundNoDHSSel':'unbound-noDHS sampled'}
        
        # plot observed/background mutation rates
        if plot == 'allTFs':
            for atype in ['boundDHS','unboundNoDHSSel', 'boundNoDHS', 'unboundNoDHS' ]:
                sdata = self.mut_TFBS_type[atype][['position',plot]][(self.mut_TFBS_type[atype]['position']>=-flank) & (self.mut_TFBS_type[atype]['position']<=flank) ]
                axss.plot(sdata['position'], sdata[plot], color=linecolors[atype], label="%s (n=%s)" % (labels[atype], sites[atype]))
            
            col="%s_mean" % plot
            for atype in ['boundDHS', 'boundNoDHS', 'unboundNoDHS']:
                sdata = self.bckg_TFBS_type[atype][['position',col]][(self.bckg_TFBS_type[atype]['position']>=-flank) & (self.bckg_TFBS_type[atype]['position']<=flank) ]
                axss.plot(sdata['position'], sdata[col], color='black', label="expected %s" % labels[atype], linestyle=linetypes[atype])
        
        # highligh the region of core TFBS motif
        size=motif/2;
        ystart, ystop = axss.get_ylim()
        axss.plot([-size, size], [ystart, ystart], color=motifColor, linestyle='-', linewidth=mscale[flank])

        # x-axis ticks control
        majorLocator   = MultipleLocator( scale[flank] )
        minorLocator   = MultipleLocator( scale[flank]/2 )
        majorFormatter = FormatStrFormatter('%d')
        axss.xaxis.set_major_locator(majorLocator)
        axss.xaxis.set_major_formatter(majorFormatter)
        axss.xaxis.set_minor_locator(minorLocator)

        # labels
        axss.set_xlabel('Distance from TFBS mid-point (nt)')
        axss.set_ylabel('Mutation rate (per nt)', color='black') 
        axss.tick_params(axis='both', which='major')
        axss.yaxis.major.locator.set_params(nbins=5) 


        if legend == 1: # legend on the right
            axss.legend(loc=1, ncol=1, fontsize=9)            
     
    def plot_TFBSxreq_tssDownstream(self, axs, target, ttype, cellType, pp, rylim ):
     
        # mutation rate
        # get the smooth curve using spline function in R
        spline = robjects.r["smooth.spline"]
        val = spline(self.mutRateTSS['position'], self.mutRateTSS['allTFs'], df=45)
        axs.plot(self.mutRateTSS['position'], self.mutRateTSS['allTFs'], alpha=0.5, color=LineColors[0])
        axs.plot(list(val[0]), list(val[1]), color=LineColors[0], lw=1 )
    
        # repair rate  
        axs2=axs.twinx()
        self.read_TFBSxrseq_tss(target, cellType, pp)
        spline = robjects.r["smooth.spline"]
        val = spline(self.repairTSS['position'], self.repairTSS['allTFs'])
        axs2.plot(list(val[0]), list(val[1]), color=FGcolor[pp], linewidth=1)
                 
        majorLocator   = MultipleLocator( 500 )
        minorLocator   = MultipleLocator( 250 )
        majorFormatter = FormatStrFormatter('%d')
        axs.xaxis.set_major_locator(majorLocator)
        axs.xaxis.set_minor_locator(minorLocator)
                                     
        axs.yaxis.major.locator.set_params(nbins=5) 
        axs2.yaxis.major.locator.set_params(nbins=5)
        axs2.set_frame_on(False)
        # highligh the region of core TFBS motif
        size=motif/2;
        ystart, ystop = axs.get_ylim()
        axs.plot([-size, size], [ystart, ystart], color=motifColor, linestyle='-', linewidth=5)
                
        if ttype == 'template':
            axs.set_title(cellTitle[cellType], loc='left', fontsize=9)
            axs.set_ylabel("Mutation rate (per nt)", color=LineColors[0])
        else:
            axs2.set_ylabel(ptitle[pp], color=FGcolor[pp])
            
        axs.set_ylim(0, 0.004)
        axs2.set_ylim(rylim)
        axs.tick_params(axis='y', color=LineColors[0], labelcolor=LineColors[0])
        axs2.tick_params(axis='y', color=FGcolor[pp], labelcolor=FGcolor[pp])
                
        if cellType == 'XPC' and pp == 'CPD':
            if ttype == 'template':
                axs2.set_title("TFBS match on template strand \n (n = 5,190)", loc='center')
            elif ttype == 'nontemplate':
                axs2.set_title("TFBS match on non-template strand \n (n = 5,396)", loc='center')
         
        if pp == 'CPD':
            axs2.axes.get_xaxis().set_ticklabels([])
        else:
            axs.set_xlabel("Distance from TFBS mid-point (nt)")
            
        if ttype == 'template':
            axs2.axes.get_yaxis().set_ticklabels([])
        else:
            axs.axes.get_yaxis().set_ticklabels([])                       

                
    def plot_TFBS_MutRate_perCtype(self, axss, ctype, flank, plot, legend):
        """ function to plot mutation rate for TFBS centered analysis
        """
        scale = { 1000:500, 100:20, 400:200, 30:10 } # xaxis-scale range
        mscale = { 1000:8, 100:8, 400:5, 30:5 } # size to highligh core TFBS bar on x-axis
        sitesCount = { 'skcm':{'DHS':74159, 'noDHS':162610}, 'mela':{'DHS':74159, 'noDHS':162610}, 'brca':{'DHS':73939, 'noDHS':162830}, 'luad':{'DHS':82608, 'noDHS':154161}, 'lusc':{'DHS':82608, 'noDHS':154161}, 'crc':{'DHS':77263, 'noDHS':159506}, 'crcHyper':{'DHS':77263, 'noDHS':159506}, 'crcHypo':{'DHS':77263, 'noDHS':159506}  }
        titles = {'skcm':'SKCM', 'mela':'MELA', 'brca':'BRCA', 'luad':'LUAD (n=46)', 'lusc':'LUSC (n=45)', 'crc':'CRC', 
                  'crcHyper':'CRC-Hypermutated (n=8)', 'crcHypo':'CRC-Hypomutated (n=34)',
                  'luad-smokers':'LUAD smokers (n=6)', 'luad-nonsmokers':'LUAD non-smokers (n=40)',
                  'blca-apobec':'BLCA-APOBEC (n=8)', 'blca-noapobec':'BLCA-no-APOBEC (n=13)', 
                  'brca-apobec':'BRCA-APOBEC (n=10)', 'brca-noapobec':'BRCA-no-APOBEC (n=86)', 
                  'hnsc-apobec':'HNSC-APOBEC (n=4)', 'hnsc-noapobec':'HNSC-APOBEC (n=23)' }
        
        # mutation rate in DHS and noDHS sites
        dataDHS = self.mut_TFBS_DHS[(self.mut_TFBS_DHS['position']>=-flank) & (self.mut_TFBS_DHS['position']<=flank) ]
        dataNoDHS = self.mut_TFBS_NODHS[ (self.mut_TFBS_NODHS['position']>=-flank) & (self.mut_TFBS_NODHS['position']<=flank) ]

        # plot observed/background mutation rates
        if plot == 'allTFs':
            spline = robjects.r["smooth.spline"]          
            axss.plot(dataDHS['position'], dataDHS[plot], color=LineColors[0], alpha=0.5, label="")
            axss.plot(dataNoDHS['position'], dataNoDHS[plot], color=LineColors[2], alpha=0.5, label="")
            val = spline(dataDHS['position'], dataDHS[plot])
            axss.plot(list(val[0]), list(val[1]), color=LineColors[0], lw=1, label="DHS")#label= "DHS (n = %s)" % "{:,}".format(sitesCount[ctype]['DHS']) , lw=1)
            val = spline(dataNoDHS['position'], dataNoDHS[plot])
            axss.plot(list(val[0]), list(val[1]), color=LineColors[2], lw=1, label="noDHS")#label="noDHS (n = %s)" % "{:,}".format(sitesCount[ctype]['noDHS']), lw=1 )
            
        # highligh the region of core TFBS motif
        size=motif/2;
        ystart, ystop = axss.get_ylim()
        axss.plot([-size, size], [ystart, ystart], color=motifColor, linestyle='-', linewidth=mscale[flank])

        # x-axis ticks control
        majorLocator   = MultipleLocator( scale[flank] )
        minorLocator   = MultipleLocator( scale[flank]/2 )
        majorFormatter = FormatStrFormatter('%d')
        axss.xaxis.set_major_locator(majorLocator)
        axss.xaxis.set_major_formatter(majorFormatter)
        if ctype in titles:
            axss.set_title("%s" % titles[ctype])
        else:
            axss.set_title("%s" % ctype)
        # labels
        if plot == 'allTFs':
            axss.xaxis.set_minor_locator(minorLocator)
                          
        axss.tick_params(axis='both', which='major')
        axss.yaxis.major.locator.set_params(nbins=4) 
        axss.yaxis.set_major_formatter(formatter)

        if legend == 1: # legend on the right
            axss.legend(loc=0, ncol=1, frameon=False, columnspacing=0)
        
        if ctype == 'crcHypo':
            axss.set_xlabel('Distance from TFBS mid-point (nt)')
        else:
            axss.axes.get_xaxis().set_ticklabels([])
       
            
    def plot_TFBS_MutRate_perMutType(self, axs, name, ctype, mutationTypes, flag):
        """Mutation rate plot for per mutation type"""
        div = 0.5
        majorLocator   = MultipleLocator( 500 )
        majorFormatter = FormatStrFormatter('%d')
        minorLocator   = MultipleLocator( 250 )

        colors = { 'CA':'#1b9e77', 'CG':'#d95f02', 'CT':'#7570b3', 'TA':'#e7298a', 'TG':'#66a61e', 'TC':'#e6ab02' }
        mutLabel = { 'CA':'C>A', 'CG':'C>G', 'CT':'C>T', 'TA':'T>A', 'TG':'T>G', 'TC':'T>C' }

        # get the smooth.spline function from R
        spline = robjects.r["smooth.spline"]
        
        for mtype in mutationTypes:
            val = spline(self.perMutType_TFBS_DHS['pos'], self.perMutType_TFBS_DHS[mtype], df = 80)
            axs.plot(list(val[0]), list(val[1]), color=colors[mtype], label=mutLabel[mtype])  #label="observed",)

        # Add a horizontal line to represent tfbs motif region
        size=motif/2;
        ystart, ystop = axs.get_ylim()
        axs.plot([-size, size], [ystart, ystart], color=motifColor, linestyle='-', linewidth=8)
        # x-axis ticks control
        axs.xaxis.set_major_locator(majorLocator)
        axs.xaxis.set_major_formatter(majorFormatter)
        #for the minor ticks, use no labels; default NullFormatter
        #axs.xaxis.set_minor_locator(minorLocator)
        axs.set_title("")
        axs.yaxis.set_major_formatter(LogFormatter(labelOnlyBase=False))
        axs.yaxis.major.locator.set_params(nbins=4) 
        axs.yaxis.set_major_formatter(formatter)
        
        if flag == 1:
            axs.legend(loc=1,ncol=2, frameon=False, columnspacing=0)
            axs.tick_params(axis='both', which='major')
        
        if ctype == 'crcHypo':
            axs.set_xlabel('Distance from TFBS mid-point (nt)')
        else:
            axs.axes.get_xaxis().set_ticklabels([])

            
    def plot_signatureEnrichment(self, axss):
        """Functions to generate enrichment plot for mutational signature analysis"""
        
        flank = 1000
        threshold = 0.01 # p-value threshold

        # add a gray line at 0 in the background
        axss.axvline(0, color='gray')

        # select results for the given cancer type
        sub = self.signature
        # high fold change and significant
        sub1 = sub[ (sub['Flank%s_chiPvalue' % flank ]<threshold) & (sub['Flank%s_FC_log2' % flank]>0) ]
        axss.plot( sub1['Flank%s_FC_log2' % flank], sub1['Flank%s_chiPvalue_neglog10' % flank], "o",   color=colors['HFCsig'], label="High FC ($P\ <$ %s)" % threshold)
        # high fold change and not significant
        sub1 = sub[ (sub['Flank%s_chiPvalue' % flank ]>=threshold) & (sub['Flank%s_FC_log2' % flank]>0) ]
        axss.plot( sub1['Flank%s_FC_log2' % flank], sub1['Flank%s_chiPvalue_neglog10' % flank ], "o",   color=colors['HFCnosig'], label="High FC ($P\ \geq$ %s)" % threshold)
        # low fold change and significant
        sub1 = sub[ (sub['Flank%s_chiPvalue' % flank ]<threshold) & (sub['Flank%s_FC_log2' % flank]<0) ]
        axss.plot( sub1['Flank%s_FC_log2' % flank], sub1['Flank%s_chiPvalue_neglog10' % flank], "o",   color=colors['LFCsig'], label="Low FC ($P\ <$ %s)" % threshold)
        # low fold change and not significant
        sub1 = sub[ (sub['Flank%s_chiPvalue' % flank ]>=threshold) & (sub['Flank%s_FC_log2' % flank]<0) ]
        axss.plot( sub1['Flank%s_FC_log2' % flank], sub1['Flank%s_chiPvalue_neglog10' % flank], "o",   color=colors['LFCnosig'], label="Low FC ($P\ \geq$ %s)" % threshold)
        # others
        sub1 = sub[ (sub['Flank%s_FC_log2' % flank] == 0) ]
        axss.plot( sub1['Flank%s_FC_log2' % flank], sub1['Flank%s_chiPvalue_neglog10' % flank], "o",   color=colors['other'], label="no FC")


        labels=sub['name'].values
        selected=sub['name'].values

        # annotated point if needed
        for label, x, y in zip(labels, sub['Flank%s_FC_log2' % flank], sub['Flank%s_chiPvalue_neglog10' % flank]):
            textfar=5
            position='left'
            if label in selected:
                #print(label)
                plt.annotate(
                    label, 
                    xy = (x, y), xytext = (-textfar, textfar),
                    textcoords = 'offset points', ha = position, va = 'bottom',
                    fontsize=10)
        
        axss.set_xlim(-2,3)
        ystart, ystop = axss.get_ylim()
        axss.set_ylim(-1, ystop)
        axss.spines['right'].set_visible(False) 
        axss.spines['top'].set_visible(False)
        # x-axis ticks control
        majorLocator   = MultipleLocator(2)
        majorFormatter = FormatStrFormatter('%d')
        axss.xaxis.set_major_locator(majorLocator)

        # Only show ticks on the left and bottom spines
        axss.yaxis.set_ticks_position('left')
        axss.xaxis.set_ticks_position('bottom')
        axss.tick_params(axis='x', direction='out')
        axss.tick_params(axis='y', direction='out')
        axss.set_ylabel("$-log_{10}$ $P$-value")
        axss.set_xlabel("$log_{2}$ Fold change")
        axss.tick_params(axis='both', which='major')