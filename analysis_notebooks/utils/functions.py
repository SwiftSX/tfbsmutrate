import os
import pandas as pd
import numpy as np
import scipy as sp
import scipy.stats
from brewer2mpl import brewer2mpl
from math import sqrt
from math import log10
from statsmodels.sandbox.stats.multicomp import multipletests as mlpt
import statsmodels.tsa.stattools as stats
import rpy2.robjects as robjects


# plot parameters
from matplotlib import pyplot as plt
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from matplotlib.ticker import LogFormatter
from matplotlib import ticker

# for y-axis formating 
formatter = ticker.ScalarFormatter(useMathText=True)
formatter.set_scientific(True) 
formatter.set_powerlimits((-1,1)) 

def fmt(x, pos):
    a, b = '{:.2e}'.format(x).split('e')
    b = int(b)
    a = a.split(".")[0]
    #a = int(a)
    return r'${} \times 10^{{{}}}$'.format(a, b)
      
def plotParam():
    plt.rcParams['font.sans-serif'] = ['arial']
    plt.rcParams['font.size'] = 10
    plt.rcParams['font.family'] = ['sans-serif']
    plt.rcParams['svg.fonttype'] = 'none'
    plt.rcParams['mathtext.fontset'] = 'custom'
    plt.rcParams['mathtext.cal'] = 'arial'
    plt.rcParams['mathtext.rm'] = 'arial'  
    
# assign colors dor different analysis
import brewer2mpl
bmap = brewer2mpl.get_map('Set1', 'qualitative', 5)
LineColors = bmap.mpl_colors
# colors for the fold enrichment plot
colors = { 'HFCsig':'#ca0020' , 'HFCnosig':'#f4a582', 'LFCsig':'#0571b0', 'LFCnosig':'#92c5de', 'other':'white' }
# colors for xr-seq photoproducts and different genomic strand
FGcolor = {'CPD':LineColors[1], 'PP64':LineColors[3], 'XPCCPD':LineColors[1], 'XPC64':LineColors[3], 'CSBCPD':LineColors[1], 'CSB64':LineColors[3] } # for combined strand
MINUScolor={'CPD':'#41b6c4', 'PP64':'#df65b0'} # for reverse strand
PLUScolor={'CPD':'#225ea8', 'PP64':'#ae017e'} # for forward strand
motif = 13 # avergae motif size
motifColor = '#ff0099' # motif color


class analysis:
    
    #### functions to read input files
    def read_TFBS_MutRate(self, ctype, target):
        """read mutation rate data of TFBS centered analysis"""    
        # observed somatic mutation rate
        self.mut_TFBS_DHS = pd.read_csv("metafiles/%s/%s_TFBS_DHS.csv.gz" % (target, ctype), header=0, sep="\t", compression='gzip', index_col=False) 
        
        if target == 'tfbs-proximal':
            self.mut_TFBS_NODHS = pd.read_csv("metafiles/%s/%s_TFBS_noDHS.csv.gz" % (target, ctype), header=0, sep="\t", compression='gzip', index_col=False)
        
            #background mutation rate data    
            self.bckg_TFBS_DHS = pd.read_csv("metafiles/%s/%s_TFBS_DHS_backg.csv.gz" % (target, ctype), header=0, sep="\t", compression='gzip', index_col=False)
            self.bckg_TFBS_NODHS = pd.read_csv("metafiles/%s/%s_TFBS_noDHS_backg.csv.gz" % (target, ctype), header=0, sep="\t", compression='gzip', index_col=False)

    def read_TFBS_MutRate_perSample(self, ctype, target):
        """read mutation rate data of TFBS centered analysis seperated by per sample"""
        self.mut_TFBS_DHS_perSample =  pd.read_csv("metafiles/%s/%s_perSample_DHS.csv.gz" % (target,ctype), header=0, sep="\t",  compression='gzip', index_col=False)
        self.mut_TFBS_NODHS_perSample = pd.read_csv("metafiles/%s/%s_perSample_noDHS.csv.gz" % (target,ctype), header=0, sep="\t",  compression='gzip', index_col=False)

    def read_TFBS_MutRate_perMutType(self, ctype):
        """read mutation rate data of TFBS centered analysis seperated by mutation type"""
        self.perMutType_TFBS_DHS = pd.read_csv("metafiles/tfbs-perMutTypes/%s_mutTypes.csv.gz" % (ctype), header=0, sep="\t", compression='gzip', index_col=False)
        
    def read_xrseq(self, region, cellType, pp, strand, target):
        """read xr-seq results that mapped to the TFBS of skcm"""
        self.xrseqValues = pd.read_csv("metafiles/%s/xr-seq/%s_%s_%s_%s.csv.gz" % (target, region, cellType, pp, strand), header=0, sep="\t", compression='gzip', index_col=False)
        
    def read_DHS_MutRate(self, ctype):
        """read mutation rate at DHS centered regions"""
        self.mut_DHSCentered = pd.read_csv("metafiles/dhsCentered/%s_DHS.csv.gz" % (ctype), header=0, sep="\t", compression='gzip', index_col=False )
        
    def read_TFBS_MutRate_distal(self, ctype, target):
        # observed mutation rate
        self.mut_TFBS_DHS_distal = pd.read_csv("metafiles/%s/%s_TFBS_DHS.csv.gz" % (target, ctype), header=0, sep="\t", compression='gzip', index_col=False) 
        # background mutation rate
        self.bckg_TFBS_DHS_distal = pd.read_csv("metafiles/%s/%s_TFBS_DHS_backg.csv.gz" % (target, ctype), header=0, sep="\t", compression='gzip', index_col=False) 
        # read nucleosome positioning data
        self.nuclSignal = pd.read_csv("metafiles/%s/allTFs_%s_DHS_nucl.csv" % (target, ctype), header=0, sep="\t")
        
    def read_additionalFiles(self):
        # tf list with motif counts and size
        self.tfList = pd.read_csv("metafiles/tf_list_withMotifSize_allCtypes.txt", sep="\t", header=None, names=['motif', 'ctype', 'size', 'counts'])

        # files for enrichment analysis Figure 1B,1C
        enrichmentAnalysis="metafiles/enrichmentAnalysis/"
        self.perTF_enrichment = pd.read_csv(enrichmentAnalysis + "perTF_FC.csv", header=0, sep="\t")
        self.perSample_enrichment = pd.read_csv(enrichmentAnalysis + "perSample_FC.csv", header=0, sep="\t")
        self.perSample_enrichment['totMutCountslog10'] = self.perSample_enrichment['totMutCounts'].apply(log10)
       
    
    #### functions to plot graphs
    def plot_TFBS_MutRate(self, axss, ctype, flank, plot, legend):
        """ function to plot mutation rate for TFBS centered analysis
        """
        scale = { 1000:200, 100:20, 400:200 } # xaxis-scale range
        mscale = { 1000:8, 100:8, 400:5 } # size to highligh core TFBS bar on x-axis
        sitesCount = { 'DHS':74159, 'noDHS':162610 } # binding site counts for proximal TFBS
        
        # Add a vertical line at the mid-point of TFBS
        if plot == "allTFs":
            axss.axvline(0, color='gray', alpha=0.5)

        # mutation rate in DHS and noDHS sites
        dataDHS = self.mut_TFBS_DHS[(self.mut_TFBS_DHS['position']>=-flank) & (self.mut_TFBS_DHS['position']<=flank) ]
        dataNoDHS = self.mut_TFBS_NODHS[ (self.mut_TFBS_NODHS['position']>=-flank) & (self.mut_TFBS_NODHS['position']<=flank) ]

        # background mutations for DHS and noDHS sites
        back = self.bckg_TFBS_DHS[ (self.bckg_TFBS_DHS['position']>=-flank) & (self.bckg_TFBS_DHS['position']<=flank) ]
        backNODHS = self.bckg_TFBS_NODHS[ (self.bckg_TFBS_NODHS['position']>=-flank) & (self.bckg_TFBS_NODHS['position']<=flank) ]

        # plot observed/background mutation rates
        if plot == 'allTFs':
            axss.plot(dataDHS['position'], dataDHS[plot], color=LineColors[0], label= "DHS (n = %s)" % "{:,}".format(sitesCount['DHS']) )
            axss.plot(dataNoDHS['position'], dataNoDHS[plot], color=LineColors[2], label="No DHS (n = %s)" % "{:,}".format(sitesCount['noDHS']) )
            axss.plot(back['position'], back["%s_mean" % plot], color='black', label="DHS expected" )
            axss.plot(backNODHS['position'], backNODHS["%s_mean" % plot], "--", color='black', label="No DHS expected" )
        else:
            axss.plot(dataDHS['position'], dataDHS[plot], color=LineColors[0], label="DHS" )
            #axss.plot(dataNoDHS['position'], dataNoDHS[plot], color=LineColors[1], label="noDHS" )
            axss.plot(back['position'], back["%s_mean" % plot], color='black', label="DHS-background" )
            #axss.plot(backNODHS['position'], backNODHS["%s_mean" % plot], "--", color='black', label="noDHS-background" )

        # highligh the region of core TFBS motif
        size=motif/2;
        ystart, ystop = axss.get_ylim()
        axss.plot([-size, size], [ystart, ystart], color=motifColor, linestyle='-', linewidth=mscale[flank])

        # x-axis ticks control
        majorLocator   = MultipleLocator( scale[flank] )
        minorLocator   = MultipleLocator( scale[flank]/2 )
        majorFormatter = FormatStrFormatter('%d')
        axss.xaxis.set_major_locator(majorLocator)
        axss.xaxis.set_major_formatter(majorFormatter)
        

        # labels
        if plot == 'allTFs':
            axss.set_xlabel('Distance from TFBS mid-point (nt)')
            axss.set_ylabel('Mutation rate (per nt)', color='black') 
            axss.xaxis.set_minor_locator(minorLocator)
                          
        axss.tick_params(axis='both', which='major')
        axss.yaxis.major.locator.set_params(nbins=4) 
        axss.yaxis.set_major_formatter(formatter)

        if legend == 1: # legend on the right
            axss.legend(loc=1, ncol=1)

        if plot != 'allTFs': # plot title for select tf    
            bindingSites = self.tfList['counts'][ (self.tfList['ctype'] == ctype) & (self.tfList['motif'] == plot) ].values[0]
            axss.set_title("%s (n = %s)" % (plot, "{:,}".format(bindingSites)))
            axss.tick_params(axis='both', which='major')
    
    def plot_TFBS_MutRate_perTF(self, axss, ctype, flank, plot, legend):
        """ function to plot mutation rate for TFBS centered analysis per TF
        """
        scale = { 1000:200, 100:20, 400:200, 30:10 } # xaxis-scale range
        mscale = { 1000:8, 100:8, 400:5, 30:5 } # size to highligh core TFBS bar on x-axis
        
        # observed mutation rate
        dataDHS = self.mut_TFBS_DHS[(self.mut_TFBS_DHS['position']>=-flank) & (self.mut_TFBS_DHS['position']<=flank) ]
        # background mutation rate
        back = self.bckg_TFBS_DHS[ (self.bckg_TFBS_DHS['position']>=-flank) & (self.bckg_TFBS_DHS['position']<=flank) ]
        

        spline = robjects.r["smooth.spline"]
        val = spline(dataDHS['position'], dataDHS[plot])
        axss.plot(dataDHS['position'], dataDHS[plot], color=LineColors[0], alpha=0.5, label="DHS" )
        axss.plot(list(val[0]), list(val[1]), color=LineColors[0], label="DHS", linewidth=1 )
        
        val = spline(back['position'], back["%s_mean" % plot])
        axss.plot(back['position'], back["%s_mean" % plot], color='black', alpha=0.5)
        axss.plot(list(val[0]), list(val[1]), color='black', linewidth=1 )
      
        # highligh the region of core TFBS motif
        size=motif/2;
        ystart, ystop = axss.get_ylim()
        axss.plot([-size, size], [ystart, ystart], color=motifColor, linestyle='-', linewidth=mscale[flank])

        # x-axis ticks control
        majorLocator   = MultipleLocator( scale[flank] )
        minorLocator   = MultipleLocator( scale[flank]/2 )
        majorFormatter = FormatStrFormatter('%d')
        axss.xaxis.set_major_locator(majorLocator)
        axss.xaxis.set_major_formatter(majorFormatter)
        axss.xaxis.set_minor_locator(minorLocator)
        
        # labels
        if plot == 'NFYA':
            axss.set_xlabel('Distance from TFBS mid-point (nt)')
        if plot == 'ETS1':
            axss.set_ylabel('Mutation rate (per nt)', color='black') 
            
                          
        axss.tick_params(axis='both', which='major')
        axss.yaxis.major.locator.set_params(nbins=4) 

        if legend == 1: # legend on the right
            axss.legend(loc=1, ncol=1)

        bindingSites = self.tfList['counts'][ (self.tfList['ctype'] == ctype) & (self.tfList['motif'] == plot) ].values[0]
        axss.set_title("%s (n = %s)" % (plot, "{:,}".format(bindingSites)))

    #### 
    def plot_EnrichmentAnalysis(self, axss, data, ctype, selected):
        """Functions to generate enrichment plot per sample and per TF analysis
        """
        flank = 1000
        threshold = 0.01 # p-value threshold
        
        # add a gray line at 0 in the background
        axss.axvline(0, color='gray')

        # select results for the given cancer type
        sub = data[data['ctype']==ctype]
        # high fold change and significant
        sub1 = sub[ (sub['Flank%s_chiPvalueAdj' % flank ]<threshold) & (sub['Flank%s_FC_log2' % flank]>0) ]
        axss.plot( sub1['Flank%s_FC_log2' % flank], sub1['Flank%s_chiPvalueAdj_neglog10' % flank], "o",   color=colors['HFCsig'], label="High FC ($P\ <$ %s)" % threshold)
        # high fold change and not significant
        sub1 = sub[ (sub['Flank%s_chiPvalueAdj' % flank ]>=threshold) & (sub['Flank%s_FC_log2' % flank]>0) ]
        axss.plot( sub1['Flank%s_FC_log2' % flank], sub1['Flank%s_chiPvalueAdj_neglog10' % flank ], "o",   color=colors['HFCnosig'], label="High FC ($P\ \geq$ %s)" % threshold)
        # low fold change and significant
        sub1 = sub[ (sub['Flank%s_chiPvalueAdj' % flank ]<threshold) & (sub['Flank%s_FC_log2' % flank]<0) ]
        axss.plot( sub1['Flank%s_FC_log2' % flank], sub1['Flank%s_chiPvalueAdj_neglog10' % flank], "o",   color=colors['LFCsig'], label="Low FC ($P\ <$ %s)" % threshold)
        # low fold change and not significant
        sub1 = sub[ (sub['Flank%s_chiPvalueAdj' % flank ]>=threshold) & (sub['Flank%s_FC_log2' % flank]<0) ]
        axss.plot( sub1['Flank%s_FC_log2' % flank], sub1['Flank%s_chiPvalueAdj_neglog10' % flank], "o",   color=colors['LFCnosig'], label="Low FC ($P\ \geq$ %s)" % threshold)
        # others
        sub1 = sub[ (sub['Flank%s_FC_log2' % flank] == 0) ]
        axss.plot( sub1['Flank%s_FC_log2' % flank], sub1['Flank%s_chiPvalueAdj_neglog10' % flank], "o",   color=colors['other'], label="no FC")


        if "TCGA" in selected[0]:
            labels=sub['sample'].values
        else:
            labels=sub['motif'].values
            
        # annotated point if needed
        for label, x, y in zip(labels, sub['Flank%s_FC_log2' % flank], sub['Flank%s_chiPvalueAdj_neglog10' % flank]):
            textfar=25
            position='left'
            if label in selected:
                #print(label)
                plt.annotate(
                    label, 
                    xy = (x, y), xytext = (-textfar, textfar),
                    textcoords = 'offset points', ha = position, va = 'bottom',
                    #bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
                    arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3'))

        axss.set_xlim(-2,5)
        ystart, ystop = axss.get_ylim()
        axss.set_ylim(-10, ystop)
        axss.spines['right'].set_visible(False) 
        axss.spines['top'].set_visible(False)
        # x-axis ticks control
        majorLocator   = MultipleLocator(2)
        majorFormatter = FormatStrFormatter('%d')
        axss.xaxis.set_major_locator(majorLocator)
        
        # Only show ticks on the left and bottom spines
        axss.yaxis.set_ticks_position('left')
        axss.xaxis.set_ticks_position('bottom')
        axss.tick_params(axis='x', direction='out')
        axss.tick_params(axis='y', direction='out')
        axss.set_ylabel("$-log_{10}$ adj. $P$-value")
        axss.set_xlabel("$log_{2}$ Fold change")
        axss.tick_params(axis='both', which='major')


    def plot_TFBS_MutRate_perSample(self, axs, sample, flag):
        """Mutation rate plot for per sample
        """
        # get the smooth.spline function from R
        spline = robjects.r["smooth.spline"]
        # for DHS regions
        val = spline(self.mut_TFBS_DHS_perSample['position'], self.mut_TFBS_DHS_perSample[sample], df = 80)
        axs.plot(list(val[0]), list(val[1]), color=LineColors[0], label="DHS")  #label="observed",) 
        # for noDHS regions
        val = spline(self.mut_TFBS_NODHS_perSample['position'], self.mut_TFBS_NODHS_perSample[sample], df = 80)
        ln1 = axs.plot(list(val[0]), list(val[1]), color=LineColors[2], label="noDHS") 

        size=motif/2;
        ystart, ystop = axs.get_ylim()
        axs.plot([-size, size], [ystart, ystart], color=motifColor, linestyle='-', linewidth=5)

        div =1 

        majorLocator   = MultipleLocator( 500 )
        majorFormatter = FormatStrFormatter('%d')
        minorLocator   = MultipleLocator( 250 )
        # x-axis ticks control
        axs.xaxis.set_major_locator(majorLocator)
        axs.xaxis.set_major_formatter(majorFormatter)
        axs.yaxis.set_major_formatter(LogFormatter(labelOnlyBase=False))
        axs.set_title("%s" % (sample), color="black", fontsize=9)
        #axs.yaxis.set_major_formatter(ticker.FuncFormatter(fmt))

        if flag == 2:   
            axs.set_xlabel('Distance from TFBS mid-point (nt)')
        else:
            axs.axes.get_xaxis().set_visible(False)

        axs.tick_params(axis='both', which='major')
        start, end = axs.get_ylim()
        if start < 0 :
            start = 0
        tickVals = np.arange(start, end, ((end-start)/3))
        axs.yaxis.set_ticks(tickVals)
    
    
    def plot_TFBS_MutRate_perMutType(self, axs, name, ctype, mutationTypes, flag):
        """Mutation rate plot for per mutation type"""
        div = 0.5
        majorLocator   = MultipleLocator( 500 )
        majorFormatter = FormatStrFormatter('%d')
        minorLocator   = MultipleLocator( 250 )

        colors = { 'CA':'#1b9e77', 'CG':'#d95f02', 'CT':'#7570b3', 'TA':'#e7298a', 'TG':'#66a61e', 'TC':'#e6ab02' }
        mutLabel = { 'CA':'C>A', 'CG':'C>G', 'CT':'C>T', 'TA':'T>A', 'TG':'T>G', 'TC':'T>C' }

        # get the smooth.spline function from R
        spline = robjects.r["smooth.spline"]
        
        for mtype in mutationTypes:
            val = spline(self.perMutType_TFBS_DHS['pos'], self.perMutType_TFBS_DHS[mtype], df = 80)
            axs.plot(list(val[0]), list(val[1]), color=colors[mtype], label=mutLabel[mtype])  #label="observed",)

        # Add a horizontal line to represent tfbs motif region
        size=motif/2;
        ystart, ystop = axs.get_ylim()
        axs.plot([-size, size], [ystart, ystart], color=motifColor, linestyle='-', linewidth=8)
        # x-axis ticks control
        axs.xaxis.set_major_locator(majorLocator)
        axs.xaxis.set_major_formatter(majorFormatter)
        #for the minor ticks, use no labels; default NullFormatter
        #axs.xaxis.set_minor_locator(minorLocator)
        axs.set_title(name)
        axs.set_xlabel('Distance from TFBS mid-point (nt)')
        axs.set_ylabel('Mutation rate (per nt)', color='black') 
        axs.yaxis.set_major_formatter(LogFormatter(labelOnlyBase=False))
        axs.yaxis.major.locator.set_params(nbins=4) 
        #axs.yaxis.set_major_formatter(ticker.FuncFormatter(fmt))
        #axs.legend(bbox_to_anchor=(1.01, 1.0), loc=2, borderaxespad=0.)
        if flag == 0:
            axs.legend(loc=1,ncol=2, frameon=False, columnspacing=0, fontsize=9)
            axs.tick_params(axis='both', which='major')
      
    def plot_TFBS_xrseq_combined(self, axs, pp, cellTypes, strand, flank, tfbs, axisParam, target): #
        """plot xrseq combined results on the TFBS centered analysis"""
        scale = { 1000:2, 100:10, 400:1, 30:1, 200:2}
        ltype = { 'NHF1':"-", 'CSB':"--", 'XPC':"-." }

        # read mutation file
        if target == 'tfbs-proximal' or target == 'tfbs-bs-seperated':
            self.read_TFBS_MutRate("skcm", target)
            dataDHS = self.mut_TFBS_DHS[(self.mut_TFBS_DHS['position']>=-flank) & (self.mut_TFBS_DHS['position']<=flank) ]
        if target == 'tfbs-distal':
            self.read_TFBS_MutRate_distal("skcm", target)
            dataDHS = self.mut_TFBS_DHS_distal[(self.mut_TFBS_DHS_distal['position']>=-flank) & (self.mut_TFBS_DHS_distal['position']<=flank) ]

      
        # get the smooth curve using spline function in R
        spline = robjects.r["smooth.spline"]
        if target == 'tfbs-distal':
            val = spline(dataDHS['position'], dataDHS[tfbs], df=50)
        else:
            val = spline(dataDHS['position'], dataDHS[tfbs])
        axs.plot(dataDHS['position'], dataDHS[tfbs], color=LineColors[0], alpha=0.5, label="DHS" )
        axs.plot(list(val[0]), list(val[1]), color=LineColors[0], label="DHS", linewidth=1.5 )

        # plot xrseq data
        axs2 = axs.twinx()
        for cell in cellTypes:
            self.read_xrseq("TFBS_DHS", cell, pp, strand, target)
            combined = self.xrseqValues[ (self.xrseqValues['position']>=-flank) & (self.xrseqValues['position']<=flank) ]
            val = spline(combined['position'], combined[tfbs])
            axs2.plot(list(val[0]), list(val[1]), color=FGcolor[pp], label=cell, linewidth=1.5, linestyle=ltype[cell] )
            

        # Add a horizontal line to represent tfbs motif region
        size=motif/2;
        axs.set_ylim(0,0.004)
        ystart, ystop = axs.get_ylim()
        axs.plot([-size, size], [ystart, ystart], color=motifColor, linestyle='-', linewidth=6)
        
        majorLocator   = MultipleLocator( flank // scale[flank] )
        majorFormatter = FormatStrFormatter('%d')
        axs.xaxis.set_major_locator(majorLocator)
        axs.xaxis.set_major_formatter(majorFormatter)
        axs.yaxis.major.locator.set_params(nbins=4) 
        axs.yaxis.set_major_formatter(formatter)
        axs.yaxis.get_offset_text().set_color(LineColors[0])
        axs.tick_params(axis='y', color=LineColors[0], labelcolor=LineColors[0])
        
        # set mutation rate y-axis limit for the plots next to each others to be comparable
        if tfbs == "allTFs":
            axs.set_ylim(0,0.0045)
        elif "allTFs_" in tfbs:
            axs.set_ylim(0,0.010)
        else:
            axs.set_ylim(0,0.012)
        
        # set title
        if len(axisParam[0]) > 0:
            if target == 'tfbs-bs-seperated':
                axs.set_title("%s" % (axisParam[0]), fontsize=9, loc='right')
            else:
                axs.set_title("%s" % (axisParam[0]), fontsize=9)
        # x-axis ticks label
        if axisParam[1] == 0:
            axs.axes.get_xaxis().set_ticklabels([])
        # x-axis label
        if axisParam[2] == 1:
            axs.set_xlabel('Distance from TFBS mid-point (nt)')
        # left y-axis ticks label
        if axisParam[3] == 0:
            axs.axes.get_yaxis().set_ticklabels([])
        # show labels
        if axisParam[4] == 1:
            axs.set_ylabel('Mutation rate (per nt)', color=LineColors[0]) 
               
        # xr-seq right side y-axis settings                   
        if pp == 'PP64':
            ystart, ystop = axs2.get_ylim()
            if tfbs == "allTFs":
                axs2.set_ylim(1.5,3)
            else:
                axs2.set_ylim(1.5,3.2)
            # right side y-axis label
            if axisParam[6] == 1:
                axs2.set_ylabel('Average (6-4)PP repair', color=FGcolor[pp])
        else:
            ystart, ystop = axs2.get_ylim()
            if tfbs == "allTFs":
                axs2.set_ylim(2,10)
            else:
                axs2.set_ylim(4,11)
            # right side y-axis label
            if axisParam[6] == 1:
                axs2.set_ylabel('Average %s repair' % pp, color=FGcolor[pp])
        
        axs2.tick_params(axis='y', color=FGcolor[pp], labelcolor=FGcolor[pp])
        axs2.yaxis.major.locator.set_params(nbins=5) 
        if axisParam[5] == 0:
                axs2.axes.get_yaxis().set_ticklabels([])
        axs2.set_frame_on(False)
        
        xlabels = axs.get_xticklabels() 
        rotation=45
        if target == 'tfbs-bs-seperated':
            rotation=90
        if target == 'tfbs-proximal':
            axs2.legend(frameon=0)
        
        for label in xlabels: 
            label.set_rotation(rotation)
            

    
    def plot_TFBS_xrseq_combined_perCellType(self, axs, pp, cell, strandTypes, flank, tfbs, axisParam, target, rylim): #
        """plot xrseq combined results on the TFBS centered analysis"""
        scale = { 1000:2, 100:10, 400:1, 30:1, 200:2}
        ltype = { 'combined':"-", 'PLUS':"-", 'MINUS':"-" }
        strandLabel = {'combined':"combined", 'PLUS':"forward", 'MINUS':"reverse" }

        # read mutation file
        if target == 'tfbs-proximal' or target == 'tfbs-bs-seperated':
            self.read_TFBS_MutRate("skcm", target)
            dataDHS = self.mut_TFBS_DHS[(self.mut_TFBS_DHS['position']>=-flank) & (self.mut_TFBS_DHS['position']<=flank) ]
        if target == 'tfbs-distal':
            self.read_TFBS_MutRate_distal("skcm", target)
            dataDHS = self.mut_TFBS_DHS_distal[(self.mut_TFBS_DHS_distal['position']>=-flank) & (self.mut_TFBS_DHS_distal['position']<=flank) ]
        
        # get the smooth curve using spline function in R
        spline = robjects.r["smooth.spline"]
        if target == 'tfbs-distal':
            val = spline(dataDHS['position'], dataDHS[tfbs], df=50)
        else:
            val = spline(dataDHS['position'], dataDHS[tfbs])
        axs.plot(dataDHS['position'], dataDHS[tfbs], color=LineColors[0], alpha=0.5, label="DHS" )
        axs.plot(list(val[0]), list(val[1]), color=LineColors[0], label="DHS", linewidth=1.5 )       

        # plot xrseq data 
        axs2 = axs.twinx()
        for strand in strandTypes:
            self.read_xrseq("TFBS_DHS", cell, pp, strand, target)
            combined = self.xrseqValues[ (self.xrseqValues['position']>=-flank) & (self.xrseqValues['position']<=flank) ]
            val = spline(combined['position'], combined[tfbs])
            if strand == 'combined':
                colorChoice=FGcolor[pp]
                axisColor=FGcolor[pp]
            elif strand == 'PLUS':  
                colorChoice=PLUScolor[pp]
                axisColor=PLUScolor[pp]
            else:
                colorChoice=MINUScolor[pp]
            #axs2.plot(combined['position'], combined[tfbs], color=colorChoice, label=strand, alpha=0.5)
            axs2.plot(list(val[0]), list(val[1]), color=colorChoice, label=strandLabel[strand], linewidth=1.5)
            

        # Add a horizontal line to represent tfbs motif region
        size=motif/2;
        axs.set_ylim(0,0.004)
        ystart, ystop = axs.get_ylim()
        axs.plot([-size, size], [ystart, ystart], color=motifColor, linestyle='-', linewidth=6)
        
        majorLocator   = MultipleLocator( flank // scale[flank] )
        minorLocator   = MultipleLocator( (flank // scale[flank])/2 )
        majorFormatter = FormatStrFormatter('%d')
        axs.xaxis.set_major_locator(majorLocator)
        axs.xaxis.set_minor_locator(minorLocator)
        axs.xaxis.set_major_formatter(majorFormatter)
        axs.yaxis.major.locator.set_params(nbins=4) 
        if target == 'tfbs-bs-seperated':
            axs.yaxis.set_major_formatter(formatter)
            axs.yaxis.get_offset_text().set_color(LineColors[0])
        axs.tick_params(axis='y', color=LineColors[0], labelcolor=LineColors[0])
        #axs.yaxis.set_major_formatter(formatter)
        
        # set mutation rate y-axis limit
        if tfbs == "allTFs":
            if target == 'tfbs-proximal' or target == 'tfbs-distal':
                axs.set_ylim(0,0.0045)
        elif "allTFs_" in tfbs:
            axs.set_ylim(0,0.010)
        else:
            axs.set_ylim(0,0.012)
        
        # set title
        if len(axisParam[0]) > 0:
            if target == 'tfbs-bs-seperated':
                axs.set_title("%s" % (axisParam[0]), fontsize=9, loc='right')
            else:
                axs.set_title("%s" % (axisParam[0]), fontsize=10)
        # set subtitle
        if len(axisParam[1]) > 0:
            axs2.set_title("%s" % (axisParam[1]), fontsize=9, loc='left')
        # x-axis ticks label
        if axisParam[2] == 0:
            axs.axes.get_xaxis().set_ticklabels([])
        # x-axis label
        if axisParam[3] == 1:
            axs.set_xlabel('Distance from TFBS mid-point (nt)')
        # left y-axis ticks label
        if axisParam[4] == 0:
            axs.axes.get_yaxis().set_ticklabels([])
        # show labels
        if axisParam[5] == 1:
            axs.set_ylabel('Mutation rate (per nt)', color=LineColors[0]) 
               
        # xr-seq right side y-axis settings                   
        if pp == 'PP64':
            if axisParam[7] == 1:
                axs2.set_ylabel('Average (6-4)PP repair', color=axisColor)
        else:
            if axisParam[7] == 1:
                axs2.set_ylabel('Average %s repair' % pp, color=axisColor)
        if len(rylim)>0:
            axs2.set_ylim(rylim[0], rylim[1])
        axs2.tick_params(axis='y', color=axisColor, labelcolor=axisColor)
        axs2.yaxis.major.locator.set_params(nbins=5) 
        if axisParam[6] == 0:
                axs2.axes.get_yaxis().set_ticklabels([])
        axs2.set_frame_on(False)
        if cell == 'NHF1' and tfbs=='allTFs':
            axs2.legend(frameon=0)
            
        xlabels = axs.get_xticklabels() 
        rotation=45
        if target == 'tfbs-proximal' or target == 'tfbs-distal':
            for label in xlabels: 
                label.set_rotation(rotation)
                
        if target == 'tfbs-bs-seperated':
            rotation=90
            for label in xlabels: 
                label.set_rotation(rotation)
            
        
            
    def plot_DHS_MutRate(self, axs, axsC2, atype, ctype, flank, flag):
        """ function to plot dhs centered mutation rate and bar plot """
  
        majorLocator   = MultipleLocator( 500  )
        majorFormatter = FormatStrFormatter('%d')
        name = atype
        atype = "%s_%s" % (ctype, atype)
        axs.axvline(0, color='gray')
        # get the smooth curve using spline function in R
        spline = robjects.r["smooth.spline"]
        val = spline(self.mut_DHSCentered['position'], self.mut_DHSCentered[atype])
        axs.plot(self.mut_DHSCentered['position'], self.mut_DHSCentered[atype], color=LineColors[0], alpha=0.5)
        axs.plot(list(val[0]), list(val[1]), color=LineColors[0], label="DHS", linewidth=1.5 )

        # Add a horizontal line to represent tfbs motif region
        size=150/2;
        ystart, ystop = axs.get_ylim()
        axs.plot([-size, size], [ystart, ystart], color=motifColor, linestyle='-', linewidth=8)
        # x-axis ticks control
        axs.xaxis.set_major_locator(majorLocator)
        axs.xaxis.set_major_formatter(majorFormatter)
        axs.set_title( "%s" % (name), fontsize=9)
        axs.yaxis.major.locator.set_params(nbins=4) 
        axs.yaxis.set_major_formatter(formatter)
        

        if name == 'DHS-Promoters-TFBS' or name == 'DHS-noPromoters-TFBS':
            axs.set_xlabel('Distance from DHS mid-point (nt)')
        else:
            axs.axes.get_xaxis().set_ticklabels([])
            
        if name == 'DHS-noPromoters-noTFBS' and flag == "selected":
            axs.set_ylabel('Mutation rate (per nt)', color='black') 
            
        if name == 'DHS-Promoters-predTFBS' and flag == "all":
            axs.set_ylabel('Mutation rate (per nt)', color='black') 
                
        if name == 'DHS-Promoters-predTFBSAll' or name == 'DHS-Promoters-predTFBS' or name == 'DHS-Promoters-noTFBS':
            axs.set_ylim(0, 0.003)

        # for bar plot
        mrate=pd.DataFrame(columns=['type','dhs','flank'])
        mrate.loc[1]=[name, (self.mut_DHSCentered[atype][ (self.mut_DHSCentered['position']>=-75) & (self.mut_DHSCentered['position']<=75) ].mean()), (self.mut_DHSCentered[atype][ (self.mut_DHSCentered['position']<-75) | (self.mut_DHSCentered['position']>75) ].mean()) ]

        ## necessary variables
        ind = np.arange(len(mrate['type'])) /2           # the x locations for the groups
        width = 0.2                      # the width of the bars

        ## the bars
        rects1 = axsC2.bar(ind, mrate['dhs'], width, color=motifColor, linewidth=0)

        rects2 = axsC2.bar(ind+width, mrate['flank'], width, color='#00B7EB', linewidth=0)

        # axes and labels
        xTickMarks = list(mrate['type'])
        axsC2.set_xticks(ind+width)
        xtickNames = axsC2.set_xticklabels(xTickMarks)
        plt.setp(xtickNames, rotation=0)
        axsC2.spines['right'].set_visible(False) 
        axsC2.spines['top'].set_visible(False)
        axsC2.yaxis.set_ticks_position('left')
        axsC2.axes.get_xaxis().set_visible(False)
        axsC2.tick_params(axis='y', direction='out')
        axsC2.yaxis.major.locator.set_params(nbins=4)
        axsC2.yaxis.set_major_formatter(ticker.FuncFormatter(fmt))
        
    def plot_DHS_xrseq_combined(self, axs, cellType, pp, strand, atype, ctype, axisParam ):
        """plot xrseq combined results on the DHS centered analysis"""

        ltype = { 'NHF1':"-", 'CSB':"--", 'XPC':"--" }
        
        # get the smooth curve using spline function in R
        col = "%s_%s" % (ctype, atype)
        spline = robjects.r["smooth.spline"]
        val = spline(self.mut_DHSCentered['position'], self.mut_DHSCentered[col])
        axs.plot(self.mut_DHSCentered['position'], self.mut_DHSCentered[col], color=LineColors[0], alpha=0.5)
        axs.plot(list(val[0]), list(val[1]), color=LineColors[0], label="DHS", linewidth=1.5 )

        # get smoth curve for xrseqvalues
        axs2 = axs.twinx()
        for cell in cellType:
            # read xrseq file
            self.read_xrseq("DHS", cell, pp, strand ,"dhsCentered")
            val = spline(self.xrseqValues['position'], self.xrseqValues[atype])
            axs2.plot(list(val[0]), list(val[1]), color=FGcolor[pp], label="combined", linewidth=1.5, linestyle=ltype[cell])

        majorLocator   = MultipleLocator( 500  )
        majorFormatter = FormatStrFormatter('%d')
     
        # Add a horizontal line to represent dhs region
        size=150/2;
        ystart, ystop = axs.get_ylim()
        #print(ctype,size,ystart)
        axs.plot([-size, size], [ystart, ystart], color=motifColor, linestyle='-', linewidth=8)
        # x-axis ticks control
        axs.xaxis.set_major_locator(majorLocator)
        axs.xaxis.set_major_formatter(majorFormatter)
        axs.yaxis.major.locator.set_params(nbins=4) 
        # y-axis format for mutation rate
        axs2.yaxis.major.locator.set_params(nbins=5) 
        axs.yaxis.set_major_formatter(formatter)
        axs.yaxis.get_offset_text().set_color(LineColors[0])

        # set title
        if len(axisParam[0]) > 0:
            axs.set_title("%s" % (axisParam[0]), fontsize=9, loc='right')
        # x-axis ticks label
        if axisParam[1] == 0:
            axs.axes.get_xaxis().set_ticklabels([])
        # x-axis label
        if axisParam[2] == 1:
            axs.set_xlabel('Distance from TFBS mid-point (nt)')
        # left y-axis ticks label
        if axisParam[3] == 0:
            axs.axes.get_yaxis().set_ticklabels([])
        # show labels
        if axisParam[4] == 1:
            axs.set_ylabel('Mutation rate (per nt)', color=LineColors[0]) 
               
        # xr-seq right side y-axis settings                   
        if pp == 'PP64':
            ystart, ystop = axs2.get_ylim()
            # right side y-axis label
            if axisParam[6] == 1:
                axs2.set_ylabel('Average (6-4)PP repair', color=FGcolor[pp])
        else:
            # right side y-axis label
            if axisParam[6] == 1:
                axs2.set_ylabel('Average %s repair' % pp, color=FGcolor[pp])
        
        if axisParam[5] == 0:
            axs2.axes.get_yaxis().set_ticklabels([])
        rotation=45
        xlabels = axs.get_xticklabels() 
        for label in xlabels: 
            label.set_rotation(rotation)
        rotation=90   
            
        axs.tick_params(axis='y', color=LineColors[0], labelcolor=LineColors[0])
        axs2.tick_params(axis='y', color=FGcolor[pp], labelcolor=FGcolor[pp])
        axs2.set_frame_on(False)
        
    def plot_DHS_xrseq_combined_perCellType(self, axs, cellType, pp, strandTypes, atype, ctype, axisParam ):
        """plot xrseq combined results on the DHS centered analysis"""
        
        strandLabel = {'combined':"combined", 'PLUS':"forward", 'MINUS':"reverse" }
        # get the smooth curve using spline function in R
        col = "%s_%s" % (ctype, atype)
        spline = robjects.r["smooth.spline"]
        val = spline(self.mut_DHSCentered['position'], self.mut_DHSCentered[col])
        axs.plot(self.mut_DHSCentered['position'], self.mut_DHSCentered[col], color=LineColors[0], alpha=0.5)
        axs.plot(list(val[0]), list(val[1]), color=LineColors[0], label="DHS", linewidth=1 )

        # get smoth curve for xrseqvalues
        axs2 = axs.twinx()
        flank=1000
        for strand in strandTypes:
            self.read_xrseq("DHS", cellType, pp, strand ,"dhsCentered")
            combined = self.xrseqValues[ (self.xrseqValues['position']>=-flank) & (self.xrseqValues['position']<=flank) ]
            val = spline(combined['position'], combined[atype])
            if strand == 'combined':
                colorChoice=FGcolor[pp]
                axisColor=FGcolor[pp]
            elif strand == 'PLUS':  
                colorChoice=PLUScolor[pp]
                axisColor=PLUScolor[pp]
            else:
                colorChoice=MINUScolor[pp]
            axs2.plot(list(val[0]), list(val[1]), color=colorChoice, label=strandLabel[strand], linewidth=1)
        
        majorLocator   = MultipleLocator( 500  )
        majorFormatter = FormatStrFormatter('%d')
     
        # set mutation rate y-axis limit
        axs.set_ylim(axisParam[8])
        axs2.set_ylim(axisParam[9])
    
        # Add a horizontal line to represent dhs region
        size=150/2;
        ystart, ystop = axs.get_ylim()
        #print(ctype,size,ystart)
        axs.plot([-size, size], [ystart, ystart], color=motifColor, linestyle='-', linewidth=5)
        
        # x-axis ticks control
        axs.xaxis.set_major_locator(majorLocator)
        axs.xaxis.set_major_formatter(majorFormatter)
        axs.yaxis.major.locator.set_params(nbins=4) 
        # y-axis format for mutation rate
        axs2.yaxis.major.locator.set_params(nbins=5) 
        axs.yaxis.set_major_formatter(formatter)
        axs.yaxis.get_offset_text().set_color(LineColors[0])
        #axs.axis.Tick(labelsize=8)
        axs.tick_params(axis='both', which='major')
        axs2.tick_params(axis='both', which='major')
        
        # set title
        if len(axisParam[0]) > 0:
            axs.set_title("%s" % (axisParam[0]), loc='right')
        # set title2
        if len(axisParam[1]) > 0:
            axs2.set_title("%s" % (axisParam[1]))
        # x-axis ticks label
        if axisParam[2] == 0:
            axs.axes.get_xaxis().set_ticklabels([])
        # x-axis label
        #if axisParam[3] == 1:
        #    axs.set_xlabel('Distance from DHS mid-point (nt)')
        # left y-axis ticks label
        if axisParam[4] == 0:
            axs.axes.get_yaxis().set_ticklabels([])
        # show labels
        if axisParam[5] == 1:
            axs.set_ylabel('Mutation rate (per nt)', color=LineColors[0]) 
               
        # xr-seq right side y-axis settings                   
        if pp == 'PP64':
            ystart, ystop = axs2.get_ylim()
            #axs2.set_ylim(ystart,3.2)
            # right side y-axis label
            if axisParam[7] == 1:
                axs2.set_ylabel('Average (6-4)PP repair', color=axisColor)
        else:
            #ystart, ystop = axs2.get_ylim()
            #axs2.set_ylim(ystart,10)
            # right side y-axis label
            if axisParam[7] == 1:
                axs2.set_ylabel('Average %s repair' % pp, color=axisColor)
        
        if axisParam[6] == 0:
            axs2.axes.get_yaxis().set_ticklabels([])
        rotation=90
        xlabels = axs.get_xticklabels() 
        for label in xlabels: 
            label.set_rotation(rotation)
        rotation=90   
            
        axs.tick_params(axis='y', color=LineColors[0], labelcolor=LineColors[0])
        axs2.tick_params(axis='y', color=axisColor, labelcolor=axisColor)
        axs2.set_frame_on(False)

            
    def normalized(self, array, x, y):
        '''function to normalize the nucleosome signals to match mutation rate'''
        # Normalize to [0, 1]:
        m = min(array);
        range = max(array) - m;
        array = (array - m) / range;
        # Then scale to [x,y]:
        range2 = y - x;
        normalized = (array*range2) + x;
        return(normalized)
    
    def plot_TFBS_MutRate_distal(self, axs1, axs2, tf):
        """plot mutation rate and nucleosome positioning signal in distal TFBS"""
        
        spline = robjects.r["smooth.spline"] 

        val1 = spline(self.mut_TFBS_DHS_distal['position'], self.mut_TFBS_DHS_distal[tf], df=50)
        # observed mutation rate actual points
        axs1.plot(self.mut_TFBS_DHS_distal['position'], self.mut_TFBS_DHS_distal[tf], color=LineColors[0], alpha=0.5, label="")

        # background mutation rate
        val = spline(self.bckg_TFBS_DHS_distal['position'], self.bckg_TFBS_DHS_distal["%s_mean"%tf])
        axs1.plot(list(val[0]), list(val[1]), color="#666666", label="expected mutations", linewidth=1)

        # observed mutation rate smoothed points
        axs1.plot(list(val1[0]), list(val1[1]), color=LineColors[0], label="observed mutations", linewidth=1 )


        axs1.set_ylabel("Mutation rate (per nt)", color='r')
        axs1.set_xlabel('Distance from TFBS mid-point (nt)')
        axs1.tick_params(axis='y', color='r', labelcolor='r')

        axss=axs1.twinx()

        # plot nucleosome signal
        # get min and max value of observed mutation rate to rescale/normalize nucleosome signal
        minVal=self.mut_TFBS_DHS_distal[tf][ (self.mut_TFBS_DHS_distal['position']<=-100) | (self.mut_TFBS_DHS_distal['position']>=100) ].min() + 0.00035 
        maxVal=self.mut_TFBS_DHS_distal[tf][ (self.mut_TFBS_DHS_distal['position']<=-100) | (self.mut_TFBS_DHS_distal['position']>=100) ].max() - 0.00035
        nuclColor="#253494"
        axss.plot(self.nuclSignal['position'], self.normalized(self.nuclSignal['Avg'], minVal, maxVal), color=nuclColor, lw=1, label="nucleosome signal")
        axss.set_ylabel('Average nucleosome signal', color=nuclColor)
        axss.tick_params(axis='y', color=nuclColor, labelcolor=nuclColor)
        ystart, ystop = axs1.get_ylim()
        axss.set_ylim(ystart,ystop)
        axss.yaxis.major.locator.set_params(nbins=4) 
        axs1.legend(loc=0, frameon=0, fontsize=9)
        axs1.yaxis.major.locator.set_params(nbins=4) 
        axs1.yaxis.set_major_formatter(formatter)
        axs1.yaxis.get_offset_text().set_color(LineColors[0])
        axss.yaxis.set_major_formatter(formatter)
        axss.yaxis.get_offset_text().set_color(nuclColor)
        axss.set_frame_on(False)


        ## plot autocorrelation
        axs2.plot(stats.acf(self.bckg_TFBS_DHS_distal['%s_mean' % tf], nlags=1000), color="#666666", label='backg. mutation rate')
        axs2.plot(stats.acf(self.mut_TFBS_DHS_distal[tf], nlags=1000), color=LineColors[0], label='mutation rate')
        axs2.plot(stats.acf(self.normalized(self.nuclSignal['Avg'], minVal, maxVal), nlags=1000), color=nuclColor, label='nucleosome signal')

        axs2.set_ylabel("Autocorrelation")
        axs2.set_xlabel("Lag (nt)")
        majorLocator   = MultipleLocator( 200 )
        majorFormatter = FormatStrFormatter('%d')
        minorLocator   = MultipleLocator( 100 )
        axs2.xaxis.set_major_locator(majorLocator)
        axs2.xaxis.set_minor_locator(minorLocator)
        axs2.xaxis.set_major_formatter(majorFormatter)
