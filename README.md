# TFBS-MutRate

This repository contains the scripts that are used to generate results presented in the article "Nucleotide excision repair is impaired by binding of transcription factors to DNA" by [Sabarinathan et al., 2016](http://dx.doi.org/10.1038/nature17661).

#### Getting data and pre-processing
[Dataset_and_Preprocessing.ipynb](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/tfbsmutrate/raw/master/Dataset_and_Preprocessing.ipynb):
This notebook contains information about the data set used in the analysis. 

#### Scripts to perform mutation rate analysis
[Scripts_Execution.ipynb](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/tfbsmutrate/raw/master/Scripts_Execution.ipynb):
This notebook explains the steps to compute mutation and repair rate at TFBS/DHS centered regions that are presented in the article. 

#### Analysis Notebooks
[analysis_notebooks/README.ipynb](http://nbviewer.ipython.org/urls/bitbucket.org/bbglab/tfbsmutrate/raw/master/analysis_notebooks/README.ipynb): 
This folder contains notebooks to visualize the source code of the main figures and Extended Data Figures presented in the article. An overview of the results/figures can be found in the README file inside.

The actual data points of all figures presented in this article can be downloaded from [here](http://bg.upf.edu/group/projects/tfbs/results/metafiles.tar.gz).


#### Acknowledgements
Thanks to Jordi Deu-Pons and Loris Mularoni for their contributions to this work.


#### Any comments or feedback, please contact
Sabarinathan Radhakrishnan (sabari.radha@upf.edu)

